package com.mic.demo.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.mic.demo.core.model.User;
import com.mic.demo.core.service.UserService;

public class ListUsersServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ServletContext sc = request.getServletContext();
		WebApplicationContext webContext = WebApplicationContextUtils
				.getWebApplicationContext(sc);
		UserService userService = (UserService) webContext
				.getBean("userService");

		List<User> users = userService.listUsers();

		// 将model数据存放在request中，转发到jsp页面时，可从request中访问model数据
		// User 就是一个JavaBean－POJO类
		// 注意reqeust｜session｜application域的生命周期
		request.setAttribute("users", users);

		RequestDispatcher rd = request
				.getRequestDispatcher("/WEB-INF/jsp/users.jsp");
		rd.forward(request, response);
	}

}
