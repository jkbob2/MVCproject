<%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="com.mic.demo.core.model.User"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<html>
<head>
<title>mic_demos_web_servlet_javabean_jsp</title>
</head>
<body>
	<%
		List<User> users = (List<User>) request.getAttribute("users");
		if (users != null) {
	%>

	<h3>系统用户列表</h3>
	<table border="1">
		<tr>
			<th>登录名</th>
			<th>显示名</th>
			<th>角色</th>
		</tr>
		<%
			Iterator iter = users.iterator();
				while (iter.hasNext()) {
					User user = (User) iter.next();
		%>
		<tr>
			<td><%=user.getUsername() %></td>
			<td><%=user.getShowname() %></td>
			<td><%=user.getRolecode() %></td>
		</tr>
		<%
			}// end while
		%>
	</table>
	<%
		} else { //end if
	%>
	<h3>没有系统用户</h3>
	<%
		}
	%>
</body>
</html>