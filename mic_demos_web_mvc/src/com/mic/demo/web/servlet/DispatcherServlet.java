package com.mic.demo.web.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.mic.demo.web.action.Action;
import com.mic.demo.web.context.ActionHandler;

public class DispatcherServlet extends HttpServlet {

	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// 得到当前Servlet的请求路径
		String servletPath = request.getServletPath();//  e.g. /listUsers
		//System.out.println(servletPath);
		String actionKey = StringUtils.replace(servletPath, "/", "");// e.g. listUser
		//System.out.println(actionKey);
		Action action = ActionHandler.getActionHandler().getAction(actionKey);
		
		String dispatchPath = "/nonaction.jsp";
		if (action != null) {
			System.out.println("Action: "+action.getClass().getCanonicalName()+" being execute.");
			String result = action.execute(request, response);
			// 视图还未实现配置化
			dispatchPath = "/WEB-INF/jsp/" + result + ".jsp";

		}
		RequestDispatcher rd = request.getRequestDispatcher(dispatchPath);
		rd.forward(request, response);

	}

}
