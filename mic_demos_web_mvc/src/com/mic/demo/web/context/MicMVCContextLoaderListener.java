package com.mic.demo.web.context;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MicMVCContextLoaderListener implements ServletContextListener {

	private String default_actionMappingLocation = "actionMappingLocation";

	@Override
	public void contextInitialized(ServletContextEvent event) {
		// TODO Auto-generated method stub
		ServletContext context = event.getServletContext();
		String actionMappingLocation = context
				.getInitParameter(default_actionMappingLocation);
		ActionHandler actionHandler = ActionHandler.getActionHandler();
		try {
			actionHandler.loadActionMapping(actionMappingLocation);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// TODO Auto-generated method stub

	}

	// getters and setters
	
	public String getDefault_actionMappingLocation() {
		return default_actionMappingLocation;
	}

	public void setDefault_actionMappingLocation(
			String default_actionMappingLocation) {
		this.default_actionMappingLocation = default_actionMappingLocation;
	}

}
