package com.mic.demo.web.context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.springframework.util.ResourceUtils;

import com.mic.demo.web.action.Action;

public class ActionHandler {

	private static ActionHandler actionHandler = null;

	private HashMap actionMap = null;

	private ActionHandler() {
		actionMap = new HashMap();
	}

	public static ActionHandler getActionHandler() {
		if (actionHandler == null) {
			actionHandler = new ActionHandler();
		}
		return actionHandler;
	}

	public void loadActionMapping(String actionMappingLocation)
			throws IOException {
		Properties prop = new Properties();
		File actionMappingFile = ResourceUtils.getFile(actionMappingLocation);
		// InputStream inStream = new FileInputStream(actionMappingLocation);
		prop.load(new FileInputStream(actionMappingFile));
		Iterator keys = prop.keySet().iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			String value = prop.getProperty(key);
			actionMap.put(key, value);
			//System.out.println(key+" = "+value);
		}
	}

	public Action getAction(String actionKey) {

		Action action = null;
		try {
			String actionClassName = (String) actionMap.get(actionKey);
			action = (Action) Class.forName(actionClassName).newInstance();
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Action no found for servletpaht: "+actionKey);
		}

		return action;

	}

}
