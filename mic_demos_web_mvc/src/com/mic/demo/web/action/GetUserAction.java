package com.mic.demo.web.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.mic.demo.core.model.User;
import com.mic.demo.core.service.UserService;

public class GetUserAction implements Action{

	public String execute(HttpServletRequest request,
			HttpServletResponse response)  {

		ServletContext sc = request.getServletContext();
		WebApplicationContext webContext = WebApplicationContextUtils
				.getWebApplicationContext(sc);
		UserService userService = (UserService) webContext
				.getBean("userService");

		User user = null;
		try {
			Integer id = Integer.valueOf((String) request.getParameter("id"));
			user = userService.getUser(id);

			// 将model数据存放在request中，转发到jsp页面时，可从request中访问model数据
			// User 就是一个JavaBean－POJO类
			// 注意reqeust｜session｜application域的生命周期
			request.setAttribute("user", user);
//servlet调用后，推出JSP页面，这是一个request周期，如果在Jsp页面需要servlet中的一些 处理结构，就从request.getAttribute中获取
//request.getAttribute("nameOfObj")可得到JSP页面一表单中控件的Value。其实表单控件中的Object的 name与value是存放在一个哈希表中的，所以在这里给出Object的name会到哈希表中找出对应它的value。
//而不同页面间传值使用request.setAttribute(position, nameOfObj)时，只会从a.jsp到b.jsp一次传递，之后这个request就会失去它的作用范围，再传就要再设一个 request.setAttribute()。而使用session.setAttribute()会在一个过程中始终保有这个值。			
		} catch (Exception e) {
		}
		return "user";

	}
	
	
}
