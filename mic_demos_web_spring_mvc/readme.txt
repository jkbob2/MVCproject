1. 项目名称: green energy (greene)
2. 代码: com.imec.greene.core, 项目核心代码, 处理内部业务逻辑；可暴露多种交互接口与客户端程序交互，比如http协议接口
3. 代码: com.imec.greene.web, 项目web应用接口, 处理与http协议的交互；
4. 代码：com.imec.greene.security, 密码函数，使用spring security 用户验证与权限
5. 代码：com.imec.greene.Utils, 项目公共函数～ 方便处理字符串等

6. 配置文件/greene/configs/
6.1 application-context.xml， spring bean容器web application级别的配置，其中import其他配置文件，实现spring bean的注册和spring security的配置
6.2 dispatcher-servlet-context.xml，spring mvc 的 DispatcherServlet配置，实现controller的注册等

7. database.properties 数据库参数，一共有两个数据库：db1 和 db2

8. configs-core.properties configs-web.properties 项目内核，web子系统的配置，暂无参数（未来可配置不同用户登录跳转不同页面等）

9. log4j.xml 日志配置

10. /greene/sql 项目数据库的导出sql文件，可仅保存创建数据库的sql，无数据的
    当前包含两个数据库的创建sql（db1和db2），测试数据以20150804的版本为止

11. 项目所有依赖jar均配置在/WebRoot/WEB-INF/lib中，可能buildpath使用的是绝对路径，需要改成相对路径，便于svn管理


12. 系统业务在权限角度，分为三大部分：请合理规划controller中的request mapping
    系统维护（url格式 /greene/admin/..）
    数据分析（url格式 /greene/opr/..）
    公众展示（url格式 /greene/pub/..）
    
    系统有三个角色：
    14.1 admin 管理员；具有系统维护，
    14.2 opr 数据分析员；具有数据分析操作权限
    14.2 pub 公众；具有公众展示权限
    
    在权限控制上，我将使得 
    /greene/admin/ 类型请求，当用户具有admin角色时，才可以访问
    /greene/opr/ 类型请求，当用户具有adminopr角色时，才可以访问
    /greene/pub/ 类型请求，当用户具有pub角色时，才可以访问
    
    一个用户，比如yan，它可有具有多个角色，比如admin，opr，pub，这样它就具有系统维护，数据分析操作权限
    
13. demo：项目有两个个controller：
    UserController: /greene/admin/list : 从数据库db1获取所有系统用户，仅有ROLE_ADMIN用户有权限查看 （管理员）
    UserController: /greene/opr/list : 从数据库db2获取所有系统设备，仅有ROLE_OPR用户有权限查看 （数据分析）
    
    /greene/login.jsp : 未登录可访问
    /greene/index.jsp : 登录后可访问
    
14. 系统用户：
    用户名   密码
    yan     yuanzheng （数据库存储密码的sha哈希值: 65e516bf851cdafdacd026500450e7b8a2e5f55d）
    zhang   yuanzheng
    chen    yuanzheng
    
14. 顺职使用voter表决器，我需要学习，才能实现，在未来可以做到更灵活－细粒度的权限控制。 
    参考：
    http://my.oschina.net/u/1177710/blog/299707

