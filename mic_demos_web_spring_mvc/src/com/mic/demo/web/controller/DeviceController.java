package com.mic.demo.web.controller;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import com.mic.demo.core.model.Device;
import com.mic.demo.core.service.db2.DeviceService;
import com.mic.demo.security.login.LoginUser;
import com.mic.demo.web.context.MicWebContext;

/**
 *  20150728
 * 
 * @author mulan
 * 
 */
@Controller("deviceController")
@RequestMapping("/opr")
public class DeviceController {

	Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	private DeviceService deviceService;

	@RequestMapping(value = "/listDevices", method = RequestMethod.GET)
	public String listDevices(ModelMap model) {

		try {
			logger.info("Accessing /listDevices!");
			
			List<Device> devices = deviceService.listDevices();
			
			/*Iterator<Device> iteratorDevices = devices.iterator();
			while(iteratorDevices.hasNext()){
				Device device = iteratorDevices.next();
				logger.info(device.toString());
			}*/
			
			model.addAttribute("devices", devices);


			//LoginUser loginUser = MicWebContext.getCurrentUser();	
			//model.addAttribute("showname", loginUser.getShowname());

			return "/demo/devices";
			
		} catch (Exception e) {
			
			//logger.error("系统错误");
			
			e.printStackTrace();
			
			return "error";
		}
	}

	// getters and setters


	public DeviceService getDeviceService() {
		return deviceService;
	}

	public void setDeviceService(DeviceService deviceService) {
		this.deviceService = deviceService;
	}

}
