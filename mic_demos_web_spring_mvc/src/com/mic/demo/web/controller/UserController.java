package com.mic.demo.web.controller;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import com.mic.demo.core.model.User;
import com.mic.demo.core.service.db1.UserService;
import com.mic.demo.security.login.LoginUser;
import com.mic.demo.web.context.MicWebContext;

/**
 * 20150728
 * 
 * @author mulan
 * 
 */
@Controller("userController")
@RequestMapping("/admin")
public class UserController {

	Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/listUsers", method = RequestMethod.GET)
	public String listUsers(ModelMap model) {

		try {
			logger.info("Accessing /listUsers!");

			List<User> users = userService.listUsers();

			Iterator<User> iteratorUsers = users.iterator();
			while (iteratorUsers.hasNext()) {
				User user = iteratorUsers.next();
				logger.info(user.toString());
			}

			model.addAttribute("users", users);

			// LoginUser loginUser = MicWebContext.getCurrentUser();
			// model.addAttribute("showname", loginUser.getShowname());

			return "/demo/users";

		} catch (Exception e) {

			// logger.error("系统错误");
			e.printStackTrace();
			return "error";
		}
	}
	
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public String addUser(User user){
		
		
		
		return "redirect:/iur/listUsers";
	}

	// getters and setters

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
