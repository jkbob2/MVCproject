package com.mic.demo.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller("iurController")
@RequestMapping("/iur")
public class IurController {
	
	protected ModelAndView getMavInstance(String path) {
		ModelAndView mav = new ModelAndView(path);

		return mav;
	}
	
	//页面跳转
		@RequestMapping(value="/coEditBaInfo", method=RequestMethod.GET)
		public ModelAndView coEditBaInfo(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/coEditBaInfo");
			return mav;
		}
		
		@RequestMapping(value="/coEditMoney", method=RequestMethod.GET)
		public ModelAndView coEditMoney(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/coEditMoney");
			return mav;
		}
		
		@RequestMapping(value="/coEditScient", method=RequestMethod.GET)
		public ModelAndView coEditScient(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/coEditScient");
			return mav;
		}
		
		@RequestMapping(value="/coViewList", method=RequestMethod.GET)
		public ModelAndView coViewList(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/coViewList");
			return mav;
		}
		
		@RequestMapping(value="/coViewPro", method=RequestMethod.GET)
		public ModelAndView coViewPro(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/coViewPro");
			return mav;
		}
		
		@RequestMapping(value="/coViewProList", method=RequestMethod.GET)
		public ModelAndView coViewProList(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/coViewProList");
			return mav;
		}
		
		@RequestMapping(value="/coViewProperty", method=RequestMethod.GET)
		public ModelAndView coViewProperty(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/coViewProperty");
			return mav;
		}
		
		@RequestMapping(value="/coViewProPlan", method=RequestMethod.GET)
		public ModelAndView coViewProPlan(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/coViewProPlan");
			return mav;
		}
		
		@RequestMapping(value="/edit", method=RequestMethod.GET)
		public ModelAndView edit(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/edit");
			return mav;
		}
		
		@RequestMapping(value="/error", method=RequestMethod.GET)
		public ModelAndView error(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/error");
			return mav;
		}

		
		@RequestMapping(value="/poEditBaInfo", method=RequestMethod.GET)
		public ModelAndView poEditBaInfo(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/poEditBaInfo");
			return mav;
		}
		
		@RequestMapping(value="/poEditDecla", method=RequestMethod.GET)
		public ModelAndView poEditDecla(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/poEditDecla");
			return mav;
		}
		
		@RequestMapping(value="/poSearchInstitution", method=RequestMethod.GET)
		public ModelAndView poSearchInstitution(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/poSearchInstitution");
			return mav;
		}
		
		@RequestMapping(value="/poView", method=RequestMethod.GET)
		public ModelAndView poView(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/poView");
			return mav;
		}
		
		@RequestMapping(value="/poViewBaInfo", method=RequestMethod.GET)
		public ModelAndView poViewBaInfo(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/poViewBaInfo");
			return mav;
		}
		
		@RequestMapping(value="/poViewList", method=RequestMethod.GET)
		public ModelAndView poViewList(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/poViewList");
			return mav;
		}
		
		@RequestMapping(value="/poAdd", method=RequestMethod.GET)
		public ModelAndView poAdd(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/poAdd");
			return mav;
		}
		
		@RequestMapping(value="/poAddAbstract", method=RequestMethod.GET)
		public ModelAndView poAddAbstract(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/poAddAbstract");
			return mav;
		}
		
		@RequestMapping(value="/poViewRegionList", method=RequestMethod.GET)
		public ModelAndView poViewRegionList(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/poViewRegionList");
			return mav;
		}
		
		@RequestMapping(value="/unEditMrInfo", method=RequestMethod.GET)
		public ModelAndView unEditMrInfo(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/unEditMrInfo");
			return mav;
		}
		
		@RequestMapping(value="/unViewList", method=RequestMethod.GET)
		public ModelAndView unViewList(ModelMap model) {
	        ModelAndView mav = getMavInstance("/iur/unViewList");
			return mav;
		}

}
