package com.mic.demo.web.context;

import java.util.Properties;

import com.mic.demo.security.login.LoginUser;
import com.mic.demo.security.login.SpringSecurityUtils;



/**
 * MIC项目web子系统上下文，提供访问 ... web子系统全局信息
 * 20150728
 * 
 * @author mulan
 *
 */
public class MicWebContext {
	
	/**
	 * 单例模式，全局仅有一个web子系统上下文
	 */
	private static MicWebContext webContext = new MicWebContext();
	
	/**
	 * 获取web子系统上下文
	 * 
	 * @return web子系统上下文
	 */
	public static final MicWebContext getGreeneWebContext(){
		return MicWebContext.webContext;
	}
	
	/**
	 * 系统web子系统配置文件，其中保存系统web子系统的各项配置信息
	 */
	private Properties webConfigs;
	
	/**
	 * 初始化web子系统配置信息
	 * @param coreConfigs
	 */
	public void initCoreConfigs(Properties webConfigs){
		this.webConfigs = webConfigs;
	}
	
	// TODO 根据项目需要，提供read｜write配置信息的接口
	
	
	/**
	 * 获取当前登录用户 or null
	 * 
	 * @return
	 */
	public static LoginUser getCurrentUser() {
		LoginUser loginUser = null;
		try {
			// 正常情况，用户通过Spring Security登录，从Spring Context中获取
			loginUser = (LoginUser) SpringSecurityUtils.getCurrentUser();
			if (loginUser != null)
				return loginUser;
			// Junit-Action 测试，登录用户没有经过Spring Security，从HttpSession里获取
			//loginUser = (LoginUser) ActionContext.getContext().getSession()
			//		.get(Utils.LOGIN_USER);
			//if (loginUser != null)
			//	return loginUser;
		} catch (Exception e) {
			// Spring session or Action session is null
		}
		return null;
	}

}
