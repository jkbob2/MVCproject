package com.mic.demo.web.context;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.context.ContextLoaderListener;

/**
 * spring web项目上下文context初始化监听器GreeneContextLoaderListener
 * 该监听器在web.xml中注册后，实现MIC项目的各项初始化工作 20150728
 * 
 * @author mulan
 * 
 */
public class MicContextLoaderListener extends ContextLoaderListener {

	public MicContextLoaderListener() {
		super();
	}

	/**
	 * 项目启动初始化工作
	 */
	@Override
	protected void customizeContext(ServletContext servletContext,
			ConfigurableWebApplicationContext applicationContext) {

		// 先进行spring容器初始化工作：super();
		super.customizeContext(servletContext, applicationContext);

		String realPath = servletContext.getRealPath(File.separator);

		// System.out.println("RealPath: " + realPath);

		// 设置log4j的参数${log4jroot}等
		String log4jroot = realPath + "WEB-INF"
				+ File.separator + "logs" + File.separator;
		String log4jinfo = log4jroot + "info" + File.separator;
		String log4jdebug = log4jroot + "debug" + File.separator;
		String log4jwarn = log4jroot + "warn" + File.separator;
		String log4jerror = log4jroot + "info" + File.separator;

		System.setProperty("log4jroot", log4jroot);
		System.setProperty("log4jinfo", log4jinfo);
		System.setProperty("log4jdebug", log4jdebug);
		System.setProperty("log4jwarn", log4jwarn);
		System.setProperty("log4jerror", log4jerror);

		// TODO 项目初始化代码

	}

	/**
	 * 项目退出销毁工作
	 */
	@Override
	public void contextDestroyed(ServletContextEvent event) {

		// 先注销spring容器
		super.contextDestroyed(event);

		// TODO 项目退出销毁代码

		System.getProperties().remove("log4jroot");
		System.getProperties().remove("log4jinfo");
		System.getProperties().remove("log4jdebug");
		System.getProperties().remove("log4jwarn");
		System.getProperties().remove("log4jerror");

	}

}
