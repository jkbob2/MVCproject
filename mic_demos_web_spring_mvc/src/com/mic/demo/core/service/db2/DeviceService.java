package com.mic.demo.core.service.db2;

import java.util.List;

import com.mic.demo.core.model.Device;



/**
 * 设备访问接口 － 
 * 20150731
 * @author mulan
 *
 */
public interface DeviceService {
	
	/**
	 * 获取所有设备
	 * 
	 * @return 设备列表
	 */
	public List<Device>  listDevices();
}
