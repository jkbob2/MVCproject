package com.mic.demo.core.service.db2;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mic.demo.core.dao.db2.DeviceDAO;
import com.mic.demo.core.model.Device;

/**
 * 设备服务接口具体实现（MyBatis）
 * 
 * @author mulan
 * 
 */
@Service("deviceServie")
public class DeviceServiceImpl implements DeviceService {

	/**
	 * 设备表Mybatis访问接口
	 */
	@Autowired
	@Qualifier("deviceDAO")
	private DeviceDAO deviceDAO;

	@Override
	public List<Device> listDevices() {

		return deviceDAO.listDevices();
	}

	// getters and setters

	public DeviceDAO getDeviceDAO() {
		return deviceDAO;
	}

	public void setDeviceDAO(DeviceDAO deviceDAO) {
		this.deviceDAO = deviceDAO;
	}

}
