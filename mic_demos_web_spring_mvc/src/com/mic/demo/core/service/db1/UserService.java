package com.mic.demo.core.service.db1;

import java.util.List;

import com.mic.demo.core.model.User;


/**
 * 用户服务接口 
 * 20150731
 * 
 * @author mulan
 *
 */
public interface UserService {
	
	/**
	 * 从当前会话上下文中获取登录用户信息
	 * 
	 * @return 返回当前登录用户
	 */
	public List<User>  listUsers();
}
