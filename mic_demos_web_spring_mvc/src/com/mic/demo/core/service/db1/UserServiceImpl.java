package com.mic.demo.core.service.db1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mic.demo.core.dao.db1.UserDAO;
import com.mic.demo.core.model.User;

 /**
  * 用户服务接口具体实现（Mybatis）
  * 20150731
  * 
  * @author mulan
  *
  */
@Service("userServie")
public class UserServiceImpl implements UserService{
	
	/**
	 * 用户表Mybatis访问接口
	 */
	@Autowired
	@Qualifier("userDAO")
	private UserDAO userDAO;

	@Override
	public List<User> listUsers() {
		// TODO Auto-generated method stub
		return userDAO.listUsers();
	}	
	
	// getters and setters

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

}
