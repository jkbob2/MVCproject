package com.mic.demo.core.context;

import java.util.Properties;

/**
 * MIC系统内核上下文，提供访问内核各项配置信息接口
 * 20150728
 * 
 * @author mulan
 *
 */
public class MicCoreContext {

	/**
	 * 单例模式，全局仅有一个内核上下文
	 */
	private static MicCoreContext coreContext = new MicCoreContext();
	
	/**
	 * 获取内核上下文
	 * 
	 * @return 内核上下文
	 */
	public static final MicCoreContext getCoreContext(){
		return MicCoreContext.coreContext;
	}
	
	/**
	 * 系统内核配置文件，其中保存系统内核的各项配置信息
	 */
	private Properties coreConfigs;
	
	/**
	 * 初始化系统核心配置信息
	 * @param coreConfigs
	 */
	public void initCoreConfigs(Properties coreConfigs){
		this.coreConfigs = coreConfigs;
	}
	
	// TODO 根据项目需要，提供read｜write配置信息的接口
	
	
	
	
}
