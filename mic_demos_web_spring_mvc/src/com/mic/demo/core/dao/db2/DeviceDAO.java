package com.mic.demo.core.dao.db2;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mic.demo.core.model.Device;



/**
 * 设备表－device 访问接口
 * 由Mybatis机制－DeviceMapper.xml配置文件具体实现
 * 
 * 20150730
 * @author mulan
 *
 */
@Component("deviceDAO")
public interface DeviceDAO {
	
	
	/**
	 * 获取所有设备列表
	 * 
	 * @return 设备列表
	 */
	public List<Device> listDevices();

}
