package com.mic.demo.core.dao.db1;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mic.demo.core.model.User;


/**
 * 用户表-user 访问接口
 * 由Mybatis机制－UserMapper.xml配置文件实现
 * 20150731
 * 
 * @author mulan
 *
 */
@Component("userDAO")
public interface UserDAO {
	
	/**
	 * 获取所有系统用户
	 * 
	 * @return 系统用户列表
	 */
	public List<User> listUsers();
	
	public void addUser(User user);

}
