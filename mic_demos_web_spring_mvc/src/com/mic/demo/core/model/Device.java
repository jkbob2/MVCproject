package com.mic.demo.core.model;

/**
 * 实体类 Device － 对应数据库表 device
 * 
 * @author mulan
 *
 */
public class Device {
	/**
	 * 主键
	 */
	private Integer id;

	/**
	 * 设备名称
	 * 
	 */
	private String devicename;


	/**
	 * 设备类型：电表 水表
	 */
	private String type;
	
	
	// getters and setters
	
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getDevicename() {
		return devicename;
	}


	public void setDevicename(String devicename) {
		this.devicename = devicename;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	@Override
	public String toString() {
		return "Device [id=" + id + ", devicename=" + devicename + ", type="
				+ type + "]";
	}
	
	
}
