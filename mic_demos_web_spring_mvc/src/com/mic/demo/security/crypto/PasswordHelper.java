package com.mic.demo.security.crypto;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
 * 用户密码工具类，用于生成用户密码的SHA或者MD5值
 * 20150731
 * 
 * @author mulan
 *
 */
public class PasswordHelper {

	/**
	 * 根据password和salt生成MD5密码
	 * 
	 * @param password 用户密码明文
	 * @param salt 随机盐
	 * @return MD5值
	 */
	public static final String getSaltedMD5Password(String password, Object salt){
		Md5PasswordEncoder md5 = new Md5PasswordEncoder();
		return md5.encodePassword(password, salt);
	}
	
	/**
	 * 根据password和salt生成密码SHA-1
	 * 
	 * @param password 用户密码明文
	 * @param salt 随机盐
	 * @return SHA-1值
	 */
	public static final String getSaltedSHAPassword(String password, Object salt){
		ShaPasswordEncoder sha = new ShaPasswordEncoder();
		return sha.encodePassword(password, salt);
	}

	public static void main(String[] args) {
		
		
		String pwd_plaintext = "yuanzheng";
		
		String pwd_sha = PasswordHelper.getSaltedSHAPassword(pwd_plaintext, null);
		
		System.out.println(pwd_sha);

	}
	
}
