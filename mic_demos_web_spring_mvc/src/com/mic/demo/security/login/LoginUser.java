package com.mic.demo.security.login;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.mic.demo.core.model.User;


/**
 * 系统登录用户，继承user实体类
 * 
 * @author mulan
 * 
 */
public class LoginUser extends User implements UserDetails, Serializable {

	// TODO 登录用户其他关联属性

	/**
	 * 登录录用的权限集合，一个登录用户可以拥有多种角色
	 */
	private Set<GrantedAuthority> authorities;

	/**
	 * 获取 登录录用的权限集合
	 */
	public Collection<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
	

	@Override
	public boolean equals(Object rhs) {
		if (rhs instanceof LoginUser)
			return this.getUsername().equals(((LoginUser) rhs).getUsername());
		return false;
	}

	@Override
	public int hashCode() {
		return this.getUsername().hashCode();
	}
}
