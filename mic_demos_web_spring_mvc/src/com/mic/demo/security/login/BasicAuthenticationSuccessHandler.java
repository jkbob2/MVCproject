package com.mic.demo.security.login;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import com.mic.demo.Utils;

/**
 * spring security安全机制，当用户登录成功后，执行该handler的onAuthenticationSuccess（）函数
 * 
 * 如有必要，可根据不同类型登录用户，跳转到不同的主页／欢迎页面～
 * 
 * @author mulan
 * 
 */
public class BasicAuthenticationSuccessHandler extends
		SimpleUrlAuthenticationSuccessHandler {

	/**
	 * 用户登录成功时被执行
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws ServletException, IOException {

		// 获取登录用户
		LoginUser loginUser = (LoginUser) SpringSecurityUtils.getCurrentUser();

		// 初始化 log4j 配置
		String userName = loginUser.getUsername();
		String userIP = request.getRemoteAddr();
		MDC.put("userName", userName);
		MDC.put("userIP", userIP);

		String default_target_url = "/index.jsp";

		// 根据角色，挑战到不同的页面，改成可配置的实现版本
		Set<String> roles = AuthorityUtils.authorityListToSet(authentication
				.getAuthorities());
		if (roles.contains(Utils.ROLE_ADMIN)) {
			default_target_url = "/admin/listUsers";
		} else if (roles.contains(Utils.ROLE_OPR)) {
			default_target_url = "/opr/listDevices";
		} else if (roles.contains(Utils.ROLE_PUB)) {
		}

		this.setDefaultTargetUrl(default_target_url);

		super.onAuthenticationSuccess(request, response, authentication);

	}
}
