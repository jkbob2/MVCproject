package com.mic.demo.security.login;

import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.google.common.collect.Sets;
import com.mic.demo.Utils;

/**
 * spring security安全机制－用于从数据库加载用户
 * 
 * @author mulan
 * 
 */
public class BasicUserDetailsServiceImpl implements UserDetailsService {

	/**
	 * 登录用户访问接口
	 */
	private LoginDAO loginDAO;


	/**
	 * 从数据库获取登录用户信息
	 */
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {

		LoginUser loginUser = null;
		try {
			// 从数据库获取用户
			loginUser = loginDAO.loadUserByUsername(username);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 用户不存在
		if (loginUser == null) {
			throw new UsernameNotFoundException("用户" + username + " 不存在!");
		}
		// 需要log用户的登录尝试
		// System.out.println(loginUser.toString());
		String rolecode = loginUser.getRolecode();
		// rolecode: ROLE_ADMIN,ROLE_OPR,ROLE_PUB
		// 默认一个用户k可有多种角色
		// 添加对默认角色的支持？
		String roles[] = rolecode.split(",");
		String code = "";
		Set<GrantedAuthority> authorities = Sets.newHashSet();
		for (int i = 0; i < roles.length; i++) {
			code = roles[i].trim();
			if (code.equalsIgnoreCase(Utils.ROLE_ADMIN)) {
				authorities.add(new SimpleGrantedAuthority(Utils.ROLE_ADMIN));
			} else if (code.equalsIgnoreCase(Utils.ROLE_OPR)) {
				authorities.add(new SimpleGrantedAuthority(Utils.ROLE_OPR));
			} else if (code.equalsIgnoreCase(Utils.ROLE_PUB)) {
				authorities.add(new SimpleGrantedAuthority(Utils.ROLE_PUB));
			}
		}

		// 设置登录用户的权限
		loginUser.setAuthorities(authorities);
		// 返回登录用户
		return loginUser;
	}

	/*
	 * Getter and Setter
	 */
	public LoginDAO getLoginDAO() {
		return loginDAO;
	}

	public void setLoginDAO(LoginDAO loginDAO) {
		this.loginDAO = loginDAO;
	}

}
