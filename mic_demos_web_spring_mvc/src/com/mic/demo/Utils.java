package com.mic.demo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;


public class Utils {
	
	/**
	 * 公众角色 PUB：public
	 */
	public static final String ROLE_PUB = "ROLE_PUB";
	
	/**
	 * 操作人员账号角色 OPR：operator
	 */
	public static final String ROLE_OPR = "ROLE_OPR";
	
	
	/**
	 * 系统管理员角色 ADMIN： admin
	 */
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	
	
	/**
	 * 将"yyyy-MM-dd"字符串转换为Date实例
	 * 
	 * @param dateStr
	 * @return Date
	 * @throws ParseException
	 */
	public static final Date convertStrToDate(String dateStr)
			throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.parse(dateStr);
	}
	
	
	/**
	 * 将Date实例转换为"yyyy-MM-dd"字符串
	 * 
	 * @param date
	 * @return String
	 */
	public static final String convertDateToStr(Date date) {
		if (date == null)
			return "";
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(date);
	}

	
	/**
	 * 检测email是否符合格式要求
	 * 
	 * @param email
	 * @return
	 */
	public static final boolean checkEmail(String email) {
		Pattern emailer = Pattern
				.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
		if (StringUtils.isBlank(email))
			return false;
		email = email.toLowerCase();
		if (email.endsWith(".con"))
			return false;
		if (email.endsWith(".cm"))
			return false;
		if (email.endsWith("@gmial.com"))
			return false;
		if (email.endsWith("@gamil.com"))
			return false;
		if (email.endsWith("@gmai.com"))
			return false;
		return emailer.matcher(email).matches();
	}
	
}
