/*
 Navicat Premium Data Transfer

 Source Server         : mulan
 Source Server Type    : MySQL
 Source Server Version : 50617
 Source Host           : localhost
 Source Database       : db1

 Target Server Type    : MySQL
 Target Server Version : 50617
 File Encoding         : utf-8

 Date: 01/14/2016 10:14:11 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `privilege`
-- ----------------------------
DROP TABLE IF EXISTS `privilege`;
CREATE TABLE `privilege` (
  `resource_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`resource_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `privilege`
-- ----------------------------
BEGIN;
INSERT INTO `privilege` VALUES ('1', '1'), ('2', '2');
COMMIT;

-- ----------------------------
--  Table structure for `resource`
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `interceptUrl` varchar(200) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `resource`
-- ----------------------------
BEGIN;
INSERT INTO `resource` VALUES ('1', '用户列表', '/admin*', null), ('2', '设备列表', '/opr*', null);
COMMIT;

-- ----------------------------
--  Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `defaultUrl` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `role`
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES ('1', 'ROLE_ADMIN', '系统管理员', '/admin/list'), ('2', 'ROLE_OPR', '数据操作员', '/opr/list');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(30) NOT NULL COMMENT '登录名',
  `showname` varchar(90) DEFAULT NULL COMMENT '显示名',
  `password` varchar(200) NOT NULL COMMENT '密码-SHA1哈希值',
  `question` varchar(45) DEFAULT NULL COMMENT '提问',
  `answer` varchar(45) DEFAULT NULL COMMENT '回答',
  `rolecode` varchar(100) NOT NULL COMMENT '用户类型{ROLE_ACAMGR|ROLE_TCH:|ROLE_STD}',
  `description` varchar(150) DEFAULT NULL COMMENT '备注',
  `lastlogintime` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用，“0-否”，“1-是，启用” 默认：1',
  `accountNonExpired` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否过期，“0-否”，“1-是，未过期” 默认：1',
  `credentialsNonExpired` tinyint(1) NOT NULL DEFAULT '1' COMMENT '密码是否失效，“0-否”，“1-是，未失效” 默认：1',
  `accountNonLocked` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否被锁定-“0-否”，“1-是，未被锁定” 默认：1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5041 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('5038', 'yan', '闫军威', 'b6edf9c902cda42a2b91f8be7643dbd9a2bf61f4', '?', '远正', 'ROLE_ADMIN,ROLE_OPR,ROLE_PUB', null, null, '1', '1', '1', '1'), ('5039', 'zhang', '张胜强', 'b6edf9c902cda42a2b91f8be7643dbd9a2bf61f4', '?', '远正智能', 'ROLE_OPR', null, null, '1', '1', '1', '1'), ('5040', 'chen', '陈春华', 'b6edf9c902cda42a2b91f8be7643dbd9a2bf61f4', '?', '远正智能', 'ROLE_PUB', null, null, '1', '1', '1', '1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
