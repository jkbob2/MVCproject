/*
 Navicat Premium Data Transfer

 Source Server         : mulan
 Source Server Type    : MySQL
 Source Server Version : 50617
 Source Host           : localhost
 Source Database       : db2

 Target Server Type    : MySQL
 Target Server Version : 50617
 File Encoding         : utf-8

 Date: 01/14/2016 10:14:02 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `device`
-- ----------------------------
DROP TABLE IF EXISTS `device`;
CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `devicename` varchar(50) NOT NULL COMMENT '名称',
  `type` varchar(20) NOT NULL COMMENT '类型－电表－水表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `device`
-- ----------------------------
BEGIN;
INSERT INTO `device` VALUES ('1', '东楼低压配电房#1主表', '电表'), ('2', '图书馆#1楼主表', '电表');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
