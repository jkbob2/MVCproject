<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
<title>政策管理-政策摘要添加</title>
<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->

<!-- build:css styles\vendor.css -->
<!-- bower:css -->
<link rel="stylesheet" href="../js/bootstrap-select/dist/css/bootstrap-select.min.css?version=20160108">
<link rel="stylesheet" href="../js/bootstrap/dist/css/bootstrap.min.css?version=20160108">
<!-- endbower -->
<!-- endbuild -->

<!-- build:css styles\main.css -->
<link rel="stylesheet" href="../css/app/main.css?version=20160108">
<link rel="stylesheet" href="../css/app/mycss.css?version=20160108">

</head>
<body>

<!--[if lt IE 10]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http:\\browsehappy.com\">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header>
      <nav class="navbar navbar-fixed-top navbar-default gradualChange" style="background-color:transparent">
        <div class="container-fluid" style="background-color:transparent">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header" style="background-color:transparent">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="#"style="background-color:transparent">首页 <span class="sr-only">(current)</span></a></li>
              <li><a href="#" style="background-color:transparent">企业管理</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">高校研究院 <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">高校</a></li>
                  <li><a href="#">科研院所</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">政策管理 <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="poSearchInstitution.html">省级行政单位</a></li>
                  <li><a href="poSearchInstitution.html">市级行政单位</a></li>
                  <li><a href="poSearchInstitution.html">区级行政单位</a></li>
                </ul>
              </li>

            </ul>
          </div><!-- \.navbar-collapse -->
        </div><!-- \.container-fluid -->
      </nav>
    </header>

    <div id="context" class="container">
    <div class="row">

    <div class="col-lg-12" id="container4context" style="margin-top:-20px">

      
    <!-- 政策基本信息修改 Editor -->
    <section id="form" class="abstractForm" style="margin-top:20px;">
  	<div class="title">
        <h3 class="text-center">摘要信息添加</h3>
    </div>
    <form action="" class="form-horizontal">

      <div class="form-group">
        <label for="" class="control-label col-xs-2">申报对象</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="广东省内注册的科研院所、高校、行政机关、企事业单位和行业组织等，应具有独立法人资格"  required>
        </div>
      </div>

      <div class="form-group">
        <label for="" class="col-xs-2 control-label">申报条件</label>
        <div class="col-sm-9">
          <textarea name="" id="" cols="70%" rows="10" class="form-control">
（一）符合“十二五”规划；符合申报指南；符合《广东省创新协同与平台环境建设专项资金管理办法》。
（二）经费预算。申报单位应认真做好项目经费预算，申请财政扶持经费的强度不得超过申报指南指引。
（三）有以下情形之一原则上不得进行申报：1.同一项目负责人或企业法人有省级科技计划项目3项以上（含3项）未完成结题的；2.在省级财政专项资金审计、检查过程中发现重大违规行为的；3.同一项目通过变换课题名称等方式进行多头申报的；4.项目主要内容已由该单位单独或联合其他单位申报并已获得省科技计划立项的；5.项目未经主管部门组织推荐的。
（四）主申报单位必须具备与申报项目内容相关的基础和能力。
          </textarea>
        </div>
        </div>
      
      <div class="form-group">
        <label for="" class="control-label col-xs-2">申报材料</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="必须按照阳光政务平台申报要求填写规定格式的申报书及项目可行性报告，申请财政支持100万元以上的技术攻关类项目要求提供查新报告，各专题指南规定的相关附件在申请时也必须同时提交。"  requried>
        </div>
      </div>
      
      <div class="form-group">
        <label for="" class="control-label col-xs-2">电子提交网站</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="阳光政务平台"  requried>
        </div>
      </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-2">电子提交时间</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="2015年3月20日下午5:00时"  requried>
        </div>
      </div>
      

      <div class="form-group">
        <label for="" class="control-label col-xs-2">纸质提交地点</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="省科技厅业务受理窗口"  requried>
        </div>
      </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-2">联系人</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="省科技厅：赵劲松、省财政厅：廖建光"  requried>
        </div>
      </div>
      
       <div class="form-group">
        <label for="" class="control-label col-xs-2">联系电话</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="020-83163832，020-83163338，83163469"  requried>
        </div>
      </div>
     

      <div class="form-group">
        <label for="" class="control-label col-xs-2">附件名称</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="2015年广东省协同创新与平台环境建设专项资金申报指南"  requried>
        </div>  
      </div>	
      
      <div class="form-group">
        <label for="" class="control-label col-xs-2">原文链接</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="http://www.gdstc.gov.cn/HTML/zwgk/tzgg/1424055399844-90239213668221994.html"  requried>
        </div>  
      </div>
      
      <div class="form-group">
        <div class="col-lg-11">
          <button class="btn btn-primary pull-right" type="button" onclick="cancel()">取消</button>
          <button class="btn btn-primary pull-right" type="button" onclick="addAbstract()" style="margin-right:1em;">确定</button>
          
        </div>
         
      </div>
      
    </form>
  </section>

      <div id="root-footer"></div>
    </div>
    </div>
    </div>


    <!--<footer>
      <p class="center">
        
        <a href="#">联系我们</a>
        <span>|</span>
        <a href="#">常见问题</a>
        <span>|</span>
        <a href="#">成功案例</a>
        <span>|</span>
      </p>
      <p>广州粤立企业顾问有限公司</p>
    </footer>-->
    <br> 
      <div class="container navbar" style="width:100%">
             <div class="row">
                <div class="col-xs-3">
                  <img src="../images/logo-name-two.png" style="width:190px;padding-left:40px" alt="">
                </div>
                
                <div class="col-xs-3">
                    <h4 style="font-weight:bold">·相关链接</h4>
                    <ul style="margin:0;padding:0">
                        <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                        <br>
                        <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                    </ul>
                </div>
                <div class="col-xs-3">
                    <h4 style="font-weight:bold">·相关链接</h4>
                    <ul style="margin:0;padding:0">
                        <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                        <br>
                        <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                       
                    </ul>
                </div>
                <div class="col-xs-3">
                    <h4 style="font-weight:bold">·相关链接</h4>
                    <ul style="margin:0;padding:0">
                        <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                        <br>
                        <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                        
                    </ul>
                </div>
            </div>
      </div>


<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/modernizr/modernizr.js"></script>
<script type="text/javascript" src="../js/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../js/app/main.js"></script>
<script type="text/javascript" src="../js/app/myjs.js"></script>

<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/affix.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/popover.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/carousel.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tab.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/select.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='https:\\www.google-analytics.com\analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

<script type="text/javascript">
  $('.selectpicker').selectpicker();
  function addAbstract(){
	  window.location = "./poView";
  }
</script>

</body>

</html>