<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
<title>基本信息</title>
<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->

<!-- build:css styles\vendor.css -->
<!-- bower:css -->
<link rel="stylesheet" href="../js/bootstrap-select/dist/css/bootstrap-select.min.css?version=20160108">
<link rel="stylesheet" href="../js/bootstrap/dist/css/bootstrap.min.css?version=20160108">
<!-- endbower -->
<!-- endbuild -->

<!-- build:css styles\main.css -->
<link rel="stylesheet" href="../css/app/main.css?version=20160108">
<link rel="stylesheet" href="../css/app/mycss.css?version=20160108">

</head>
<body>

<!--[if lt IE 10]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http:\\browsehappy.com\">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header>
  <nav class="navbar navbar-fixed-top navbar-default " style="background-color:transparent">
    <div id="nav_sec" class="container-fluid gradualChange" style="background-color:transparent">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header" style="background-color:transparent">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">band</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="#"style="background-color:transparent">首页 <span class="sr-only">(current)</span></a></li>
          <li><a href="#" style="background-color:transparent">企业管理</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">高校研究院 <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">高校</a></li>
              <li><a href="#">科研院所</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">政策管理 <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="poSearchInstitution.html">省级行政单位</a></li>
              <li><a href="poSearchInstitution.html">市级行政单位</a></li>
              <li><a href="poSearchInstitution.html">区级行政单位</a></li>
            </ul>
          </li>

        </ul>
      </div><!-- \.navbar-collapse -->
    </div><!-- \.container-fluid -->
    <div class="container" style="text-align:center;width:90%;">
      <div class="row">
      <div class="col-lg-10 col-lg-offset-2">
      <div style="margin:0 -15px;" id="nav_id"></div>
      </div>
      </div>
    </div>
  </nav>
</header>

<div id="context" class="container" style="width:90%">
  <div class="row">
  
    <div class="col-lg-2 bs-docs-sidebar">
      <ul class="nav nav-list bs-docs-sidenav affix">
        <li class="active"><a href="#bs-example-navbar-collapse-1"><i class="icon-chevron-right"></i> 导航搜索</a></li>
        <li><a href="#view" onclick="divShow()"><i class="icon-chevron-right"></i> 机构信息</a></li>
        <li><a href="#table"><i class="icon-chevron-right"></i> 政策列表</a></li>
        <li><a href="#forms"><i class="icon-chevron-right"></i> ###</a></li>
        <li><a href="#buttons"><i class="icon-chevron-right"></i> ###</a></li>
      </ul>
    </div>
  
  <div class="col-lg-10" id="container4context" style="margin-top:-20px">
  <section id="search-box" class="row" style="margin-top:2em">
    <div class="col-lg-9">
      <select class="selectpicker" data-width="auto">
        <option>中国</option>
      </select>
      <select class="selectpicker" data-width="auto">
        <option>广东省</option>
      </select>
      <select class="selectpicker" data-width="auto">
        <option>全部</option>
        <option>广州市</option>
      </select>
      <select class="selectpicker" data-width="auto">
        <option>全部</option>>
        <option>天河区</option>
        <option>番禺区</option>
        <option>越秀区</option>
        <option>黄浦区</option>
        <option>白云区</option>
      </select>
      <select class="selectpicker" data-width="auto">
        <option>科学技术厅</option>
      </select>
      <select class="selectpicker" data-width="auto">
        <option>规划财务处</option>
      </select>
    </div>
    <div class="col-lg-3 pull-right">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="输入关键字搜索">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">搜索</button>
        </span>
      </div><!-- \input-group -->
    </div><!-- \.col-lg-6 -->
  </section>

  <section id="view" style="margin-top:1em">
    <div id="name" class="title" style="height:100px;">
        <h3 class="text-center" style="font-size: 30px">广东省科学技术厅规划财务处</h3>
        <h4 style="float:left; padding:0px 0px 1px 66%; color:#ffffff; font-size: 15px"><span class="glyphicon glyphicon-user"></span>020-0000001&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-envelope"></span>888yueli@vciur.com</h4>
    </div>
  
    
    <div id="message" style="visibility:none; display:none;" class="main-info info" id="">
      <ul>
        <li class="info-title"><h3>机构信息</h3></li>
      </ul>
   
      <table class="table table-hover table-border table-striped">
      <tbody>
        <tr>
          <td style="font-weight:bold" class="col-lg-2">机构地址：</td>
          <td>广东省广州市番禺区**</td>
        </tr>
        <tr>
          <td style="font-weight:bold">联系方式：</td>
          <td>020-0000001</td>
        </tr>
        <tr>
          <td style="font-weight:bold">电子邮箱：</td>
          <td>888yueli@vciur.com</td>
        </tr>
        </tbody>
        </table>
      
    </div>
  </section>

  <section id="table">
   <div class="clearfix"></div>
   <div class="main-info info">
    <ul style="padding:0px;list-style:none" >
      <li class="info-title"><h3>政策列表</h3><button class="btn btn-sm btn-primary pull-right">添加政策</button></li>
    
       
    </ul>
    <table id="table_sort" class="table table-hover table-border table-striped">
      <thead>
        <th>#</th>
        <th>政策标题</th>
        <th>发布时间</th>
        <th>有效期</th>
        <th>文号</th>
        <th>操作
          <!--<a href="#">新增政策</a>-->
        </th> 
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td class="col-lg-5"><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
          <td>2015/11/12</td>
          <td>自政策发布起一年内</td>
          <td>粤科函规财字（2015）190号</td>
          <td>
            <button class="btn btn-xs btn-default" type="button" onclick="location='edit4zc.html'">编辑</button>
            <span>|</span>
            <button class="btn btn-xs btn-danger" type="button" disabled>删除</button>
          </td>
        </tr>


        
      </tbody>
    </table>
   </div> 
  </section>

  <section>

    <nav class="pull-right">
      <ul class="pagination">
        <li>
          <a href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li>
          <a href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
  </section>
  </div> 

 </div>
</div>

<div class="container navbar" style="width:100%">
         <div class="row">
            <div class="col-xs-3">
              <img src="../images/logo-name-two.png" style="width:190px;padding-left:40px" alt="">
            </div>
            
            <div class="col-xs-3">
                <h4 style="font-weight:bold">·相关链接</h4>
                <ul style="margin:0;padding:0">
                    <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                    <br>
                    <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                </ul>
            </div>
            <div class="col-xs-3">
                <h4 style="font-weight:bold">·相关链接</h4>
                <ul style="margin:0;padding:0">
                    <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                    <br>
                    <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                   
                </ul>
            </div>
            <div class="col-xs-3">
                <h4 style="font-weight:bold">·相关链接</h4>
                <ul style="margin:0;padding:0">
                    <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                    <br>
                    <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                    
                </ul>
            </div>
        </div>
  </div>


<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/modernizr/modernizr.js"></script>
<script type="text/javascript" src="../js/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../js/app/main.js"></script>
<script type="text/javascript" src="../js/app/myjs.js"></script>

<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/affix.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/popover.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/carousel.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tab.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/select.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='https:\\www.google-analytics.com\analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

<script type="text/javascript">
  $('.selectpicker').selectpicker();
</script>

</body>

</html>