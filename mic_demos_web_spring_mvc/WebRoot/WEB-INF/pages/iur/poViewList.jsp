<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
<title>政策管理-政策列表</title>
<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->

<!-- build:css styles\vendor.css -->
<!-- bower:css -->
<link rel="stylesheet" href="../js/bootstrap-select/dist/css/bootstrap-select.min.css?version=20160108">
<link rel="stylesheet" href="../js/bootstrap/dist/css/bootstrap.min.css?version=20160108">
<!-- endbower -->
<!-- endbuild -->

<!-- build:css styles\main.css -->
<link rel="stylesheet" href="../css/app/main.css?version=20160108">
<link rel="stylesheet" href="../css/app/mycss.css?version=20160108">

</head>
<body>

<!--[if lt IE 10]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header>
  <nav class="navbar navbar-fixed-top navbar-default" style="background-color:transparent">
    <div  id="nav_sec" class="container-fluid gradualChange" style="background-color:transparent">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header" style="background-color:transparent">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Brand</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="#"style="background-color:transparent">首页 <span class="sr-only">(current)</span></a></li>
          <li><a href="#" style="background-color:transparent">企业管理</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">高校研究院 <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">高校</a></li>
              <li><a href="#">科研院所</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="poSearchInstitution.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">政策管理 <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="poSearchInstitution">省级行政单位</a></li>
              <li><a href="poSearchInstitution">市级行政单位</a></li>
              <li><a href="poSearchInstitution">区级行政单位</a></li>
            </ul>
          </li>

        </ul>
      </div><!-- \.navbar-collapse -->
    </div><!-- \.container-fluid -->
    <div class="container" style="text-align:center; ">
      <div class="row" id="nav_id">
      </div>
    </div>
  </nav>
</header>

<br>
<div id="context" class="container">
  <section id="search-box" class="row">
    <div class="col-lg-8">
      <select class="selectpicker" data-width="auto">
        <option>中国</option>
      </select>
      <select class="selectpicker" data-width="auto">
        <option>广东省</option>
      </select>
      <select class="selectpicker" data-width="auto">
        <option>广州市</option>
      </select>
      <select class="selectpicker" data-width="auto">
        <option>天河区</option>
        <option>番禺区</option>
        <option>越秀区</option>
        <option>黄浦区</option>
        <option>白云区</option>
      </select>
    </div>
    <div class="col-lg-3 pull-right">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">Go!</button>
        </span>
      </div><!-- /input-group -->
    </div><!-- /.col-lg-6 -->
  </section>
  

  <section id="view" style="margin-top:1em" class="row" >
  <div id="name" class="title col-xs-12">
      <h3 class="text-center">政策列表</h3>
      </div>
      </section>
      <br>
  <section id="table">
    <button class="btn btn-primary pull-right" onclick="addPolicy()">添加政策</button>

    <table class="table table-hover table-border">
      <thead>
        <th>#</th>
        <th>机构</th>
        <th>办公室</th>
        <th>政策名称</th>
        <th>时间</th>
        <th>操作
          <!--<a href="#">新增政策</a>-->
        </th> 
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>2</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>3</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>4</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>5</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>6</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>7</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>8</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>9</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>10</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>11</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>12</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>13</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr><tr>
          <td>14</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr><tr>
          <td>15</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr><tr>
          <td>16</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>17</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>18</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>19</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>20</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
        <tr>
          <td>21</td>
          <td>区政府办公室</td>
          <td>某某办公室</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td>2015-09-02</td>
          <td>
            <a href="./poEditBaInfo">编辑</a>
            <span>|</span>
            <a href="./poView">查看</a>
          </td>
        </tr>
      </tbody>
    </table> 
  </section>
  <section>

    <nav class="pull-right">
      <ul class="pagination">
        <li>
          <a href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li>
          <a href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
  </section>

  <div id="root-footer"></div>
</div>
<br> 
  <div class="container navbar" style="width:100%">
         <div class="row">
            <div class="col-xs-3">
              <img src="../images/logo-name-two.png" style="width:190px;padding-left:40px" alt="">
            </div>
            
            <div class="col-xs-3">
                <h4 style="font-weight:bold">·相关链接</h4>
                <ul style="margin:0;padding:0">
                    <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                    <br>
                    <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                </ul>
            </div>
            <div class="col-xs-3">
                <h4 style="font-weight:bold">·相关链接</h4>
                <ul style="margin:0;padding:0">
                    <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                    <br>
                    <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                   
                </ul>
            </div>
            <div class="col-xs-3">
                <h4 style="font-weight:bold">·相关链接</h4>
                <ul style="margin:0;padding:0">
                    <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                    <br>
                    <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                    
                </ul>
            </div>
        </div>
  </div>


<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/modernizr/modernizr.js"></script>
<script type="text/javascript" src="../js/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../js/app/main.js"></script>
<script type="text/javascript" src="../js/app/myjs.js"></script>

<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/affix.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/popover.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/carousel.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tab.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/select.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='https:\\www.google-analytics.com\analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

<script type="text/javascript">
  $('.selectpicker').selectpicker();
  function addPolicy(){
	  window.location = "./poAdd";
  }
</script>

</body>

</html>