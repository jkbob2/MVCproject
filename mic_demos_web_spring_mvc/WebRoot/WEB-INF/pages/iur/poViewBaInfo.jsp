<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
<title>政策管理-新政策信息</title>
<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->

<!-- build:css styles\vendor.css -->
<!-- bower:css -->
<link rel="stylesheet" href="../js/bootstrap-select/dist/css/bootstrap-select.min.css?version=20160108">
<link rel="stylesheet" href="../js/bootstrap/dist/css/bootstrap.min.css?version=20160108">
<!-- endbower -->
<!-- endbuild -->

<!-- build:css styles\main.css -->
<link rel="stylesheet" href="../css/app/main.css?version=20160108">
<link rel="stylesheet" href="../css/app/mycss.css?version=20160108">

</head>
<body>

<!--[if lt IE 10]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header>
  <nav class="navbar navbar-fixed-top navbar-default" style="background-color:transparent">
    <div id="nav_sec" class="container-fluid gradualChange" style="background-color:transparent">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header" style="background-color:transparent">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Brand</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="#"style="background-color:transparent">首页 <span class="sr-only">(current)</span></a></li>
          <li><a href="#" style="background-color:transparent">企业管理</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">高校研究院 <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">高校</a></li>
              <li><a href="#">科研院所</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">政策管理 <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="poSearchInstitution">省级行政单位</a></li>
              <li><a href="poSearchInstitution">市级行政单位</a></li>
              <li><a href="poSearchInstitution">区级行政单位</a></li>
            </ul>
          </li>

        </ul>
      </div><!-- \.navbar-collapse -->
    </div><!-- \.container-fluid -->
    <div class="container" style="text-align:center;width:90%;">
      <div class="row">
      <div class="col-lg-10 col-lg-offset-2">
      <div style="margin:0 -15px;" id="nav_id"></div>
      </div>
      </div>
    </div>
  </nav>
</header>

<div id="context" class="container" style="width:90%">

  <div class="row">
  
    <div class="col-lg-2 bs-docs-sidebar">
      <ul class="nav nav-list bs-docs-sidenav affix" id="navigate">
        <li class="active"><a href="#"><i class="icon-chevron-right"></i>基本信息</a></li>
        <li><a href="#abstract"><i class="icon-chevron-right"></i>申报信息</a></li>
        <li><a href="#attachment"><i class="icon-chevron-right"></i>附件列表</a></li>

      </ul>
    </div>
  
  <div class="col-lg-10" id="container4context">

  <section id="view" style="margin-top:1em" class="row" >
	  <div id="name" class="title">
	        <h3 class="text-center">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</h3>
	  </div>
  </section>
  
  <section id="view" class="info" style="margin-top:1em">
    <div class="subtitle">
        <h3 class="text-center">政策法规基本信息表</h3>
    </div>
    <div class="clearfix"></div>
    <div class="main-info info" id="">
      <table class="table table-hover table-border table-striped">
      <tbody>
        <tr>
          <td style="font-weight:bold" class="col-lg-2">名称：</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
        </tr>
        <tr>
          <td style="font-weight:bold">地区：</td>
          <td>广东省</td>
        </tr>
        <tr>
          <td style="font-weight:bold">印发部门：</td>
          <td>广东省科学技术厅规划财务处</td>
        </tr>
        <tr>
          <td style="font-weight:bold">相关部门：</td>
          <td>广东省财政厅</td>
        </tr>
        <tr>
          <td style="font-weight:bold">文号：</td>
          <td>粤科函规财字【2015】190号</td>
        </tr>
        <tr>
          <td style="font-weight:bold">印发时间：</td>
          <td>2015/02/16</td>
        </tr>
        <tr>
          <td style="font-weight:bold">有效时间：</td>
          <td>2015/03/31</td>
        </tr>
        <tr>
          <td style="font-weight:bold">关键字：</td>
          <td>协同创新、平台环境、专项资金、科技创新</td>
        </tr>
        <tr>
          <td style="font-weight:bold">摘要：</td>
          <td><textarea name="" id="" cols="57%" rows="3" cols="20" readonly="readonly" class="form-control">为了进一步建设完善科技创新平台，营造有利于创新的良好环境，省科技厅、省财政厅决定联合组织申报2015年度广东省协同创新与平台环境建设专项资金项目······</textarea></td>
        </tr>
        
        <tr>
          <td style="font-weight:bold">内容：</td>
          <td><textarea name="" id="" cols="57%"  rows="20" cols="20" readonly="readonly" class="form-control">
          各地级以上市科技局（委）、财政局（委），顺德区经济和科技促进局、财税局，省直有关部门，有关单位：
为贯彻党的十八届三中全会和省委十一届四次会议精神，落实《中共广东省委广东省人民政府关于全面深化科技体制改革加快创新驱动发展的决定》，按照《广东省协同创新与平台环境建设专项资金管理办法》有关要求，省科技厅、省财政厅决定联合组织申报2015年度省广东省协同创新与平台环境建设专项资金项目（产学研合作专项领域项目另行组织申报），进一步建设完善科技创新平台，营造有利于创新的良好环境。现将《2015年广东省协同创新与平台环境建设专项资金申报指南》印发给你们，并将有关申报事项通知如下：
一、组织方式
本专项资金项目原则上采取自由申报的组织形式，经专家评审后择优扶持。所有项目申报必须通过省财政专项资金管理平台或省科技业务管理阳光政务平台（http：//pro.gdstc.gov.cn）进行申报及提交有关申请资料。
二、申报要求
（一） 项目符合国家和省科学技术“十二五”规划；符合申报指南的有关要求；符合《广东省创新协同与平台环境建设专项资金管理办法》有关要求。
（二） 本专项申报单位原则上为广东省内注册的科研院所、高校、行政机关、企事业单位和行业组织等，应具有独立法人资格。具体指南另有要求的，须一并遵循。
（三） 申报单位具有良好的工作基础，鼓励支持产学研合作项目，优先支持获得PCT专利、发明专利等自主知识产权项目。
（四） 项目申报资料。项目申报必须按照阳光政务平台申报要求填写规定格式的申报书及项目可行性报告，申请财政支持100万元以上的技术攻关类项目要求提供查新报告，各专题指南规定的相关附件在申请时也必须同时提交，未按规定提交者，视为形式审查不合格。项目申报单位及申报人可根据需要提交其他相关附件，原则上附件材料应能够对项目申报内容和方案进行佐证、说明项目申报单位实施项目的有利条件（如企业须提供经第三方审计的财务报表，对投资额度大的项目说明资金筹措来源及依据，提供项目执行团队以往完成的科研成果证明等）。各单位须对所有申报资料的真实性负责，所有申报资料均必须真实可靠，项目申报单位报送纸质材料时，须提供申报材料真实性承诺函（可与其他材料一并装订）。
（五） 经费预算。申报单位应认真做好项目经费预算，申请财政扶持经费的强度不得超过申报指南指引。指南中未做特别说明的，原则上研究院所、高校、非营利性机构申请的项目为事前立项下达，事前拨付经费；企业申请的项目为事前立项下达、项目完成并验收通过后拨付经费。
（六） 有以下情形之一的项目负责人或申报单位原则上不得进行申报或通过资格审查：1.同一项目负责人或企业法人有省级科技计划项目3项以上（含3项）未完成结题的；2.在省级财政专项资金审计、检查过程中发现重大违规行为的；3.同一项目通过变换课题名称等方式进行多头申报的；4.项目主要内容已由该单位单独或联合其他单位申报并已获得省科技计划立项的；5.项目未经主管部门组织推荐的。
（七） 主申报单位必须具备与申报项目内容相关的基础和能力，项目申报书的内容、指标必须明确客观，严禁各种夸大和作假行为。项目如获得立项要确保申报书和合同书的内容、指标的一致性，违者按有关规定严肃处理。
三、申报程序
（一） 注册。首次申报的单位可在省专项资金管理统一平台进行注册后转入省科技业务管理阳光政务平台进行申报；或者在省科技业务管理阳光政务平台注册单位信息，获得单位用户名和密码，同时获得为本单位项目申报人开设用户帐号的权限，项目主持人从单位科研管理人员处获得用户名和密码，填写个人信息后进行申报。已注册的单位继续使用原有帐号进行申报和管理。
（二） 申报。各单位和申报人注册后即可通过网络提交申请书及相关材料，并在主管部门审核通过后打印书面申报书一式1份（含通过系统上传的所有附件和真实性承诺函原件）送交所属主管部门，由主管部门汇总审核后统一交科技厅业务受理窗口。
（三） 审核推荐。各级主管部门在省科技业务管理阳光政务平台对申报项目择优推荐，并正式行文（含推荐项目汇总表）分别报送省科技厅、省财政厅。其中各地级以上市所属企事业单位的申报项目，必须由地级以上市科技局和财政局联合行文报送；其余省直等相关部门所属企事业单位的申报项目，由主管部门行文报送。
（四） 资格审查和项目评审。省科技厅会同省财政厅组织专家，对各主管部门推荐的项目进行资格审查和评审，按照竞争择优的原则列入科技计划予以支持。
四、申报时间
申报单位网上申报及提交截止时间为2015年3月20日下午5:00时，各级科技主管部门网上审核推荐截止时间为2015年3月27 日下午5:00时。书面申报材料送省科技厅业务受理窗口的截止时间为2015年3月31日下午5:00时。
五、联系方式
书面材料报送地址：广州市连新路171号省科技信息大楼1楼广东省科技厅业务受理窗口（邮政编码：510033）
（一）省科技厅：
规划财务处 赵劲松，电话：020-83163832（综合性业务咨询）
广东省科技创新监测研究中心（技术支持）电话：020-83163338、83163469
各专题业务性问题请向申报指南所列联系人咨询。
（二） 省财政厅：
教科文处 廖建光，电话：020-83170070（财政资金管理服务）。
				
																		省科技厅 省财政厅
																		2015年2月16日
				</textarea>
        </td>
        </tr>
        
       
      </tbody>
    
    </table>
    
    </div>
    
    <div class="modify">
           <button id="infoModify" onclick="modify(1)">修改</button>
           
   	</div>
    </section>
    
    
    
    <!-- 政策基本信息修改 Editor -->
    <section id="form" class="infoForm" style="margin-top:20px; display:none;">
  	<div class="subtitle">
        <h3 class="text-center">基本信息修改</h3>
    </div>
    <form action="" class="form-horizontal">

      <div class="form-group">
        <label for="" class="control-label col-xs-2">政策名称</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知"  required>
        </div>
      </div>

      <div class="form-group">
        <label for="" class="col-xs-2 control-label">发布单位</label>
        <div class="col-sm-9">
          <select class="selectpicker" data-width="auto">
            <option>中国</option>
          </select>
          <select class="selectpicker" data-width="auto">
            <option>广东省</option>
          </select>
          <select class="selectpicker" data-width="auto">
            <option>广州市</option>
          </select>
          <select class="selectpicker" data-width="auto">
            <option>天河区</option>
            <option>番禺区</option>
            <option>越秀区</option>
            <option>黄浦区</option>
            <option>白云区</option>
          </select>
          <select name="" id="" class="selectpicker" data-width="auto">
            <option>某某办公室</option>
          </select>
          <select name="" id="" class="selectpicker" data-width="auto">
            <option value="">某某机构</option>
          </select>
        </div>
        </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-2">相关机构</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="广东省财政厅"  requried>
        </div>
      </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-2">政策文号</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="粤科函规财字【2015】190号"  requried>
        </div>
      </div>
      

      <div class="form-group">
        <label for="" class="control-label col-xs-2">印发时间</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="2015/02/16"  requried>
        </div>
      </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-2">有效时间</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="2015/03/31"  requried>
        </div>
      </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-2">关键字</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" value="协同创新、平台环境、专项资金、科技创新"  requried>
        </div>
      </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-2">摘要</label>
        <div class="col-sm-9">
          <textarea name="" id="" cols="70%" rows="6" class="form-control">为了进一步建设完善科技创新平台，营造有利于创新的良好环境，省科技厅、省财政厅决定联合组织申报2015年度广东省协同创新与平台环境建设专项资金项目······</textarea>
        </div>
      </div>
      
      <div class="form-group">
        <label for="" class="control-label col-xs-2">内容</label>
        <div class="col-sm-9">
          <textarea name="" id="" cols="70%" rows="10" class="form-control">
          各地级以上市科技局（委）、财政局（委），顺德区经济和科技促进局、财税局，省直有关部门，有关单位：
为贯彻党的十八届三中全会和省委十一届四次会议精神，落实《中共广东省委广东省人民政府关于全面深化科技体制改革加快创新驱动发展的决定》，按照《广东省协同创新与平台环境建设专项资金管理办法》有关要求，省科技厅、省财政厅决定联合组织申报2015年度省广东省协同创新与平台环境建设专项资金项目（产学研合作专项领域项目另行组织申报），进一步建设完善科技创新平台，营造有利于创新的良好环境。现将《2015年广东省协同创新与平台环境建设专项资金申报指南》印发给你们，并将有关申报事项通知如下：
一、组织方式
本专项资金项目原则上采取自由申报的组织形式，经专家评审后择优扶持。所有项目申报必须通过省财政专项资金管理平台或省科技业务管理阳光政务平台（http：//pro.gdstc.gov.cn）进行申报及提交有关申请资料。
二、申报要求
（一） 项目符合国家和省科学技术“十二五”规划；符合申报指南的有关要求；符合《广东省创新协同与平台环境建设专项资金管理办法》有关要求。
（二） 本专项申报单位原则上为广东省内注册的科研院所、高校、行政机关、企事业单位和行业组织等，应具有独立法人资格。具体指南另有要求的，须一并遵循。
（三） 申报单位具有良好的工作基础，鼓励支持产学研合作项目，优先支持获得PCT专利、发明专利等自主知识产权项目。
（四） 项目申报资料。项目申报必须按照阳光政务平台申报要求填写规定格式的申报书及项目可行性报告，申请财政支持100万元以上的技术攻关类项目要求提供查新报告，各专题指南规定的相关附件在申请时也必须同时提交，未按规定提交者，视为形式审查不合格。项目申报单位及申报人可根据需要提交其他相关附件，原则上附件材料应能够对项目申报内容和方案进行佐证、说明项目申报单位实施项目的有利条件（如企业须提供经第三方审计的财务报表，对投资额度大的项目说明资金筹措来源及依据，提供项目执行团队以往完成的科研成果证明等）。各单位须对所有申报资料的真实性负责，所有申报资料均必须真实可靠，项目申报单位报送纸质材料时，须提供申报材料真实性承诺函（可与其他材料一并装订）。
（五） 经费预算。申报单位应认真做好项目经费预算，申请财政扶持经费的强度不得超过申报指南指引。指南中未做特别说明的，原则上研究院所、高校、非营利性机构申请的项目为事前立项下达，事前拨付经费；企业申请的项目为事前立项下达、项目完成并验收通过后拨付经费。
（六） 有以下情形之一的项目负责人或申报单位原则上不得进行申报或通过资格审查：1.同一项目负责人或企业法人有省级科技计划项目3项以上（含3项）未完成结题的；2.在省级财政专项资金审计、检查过程中发现重大违规行为的；3.同一项目通过变换课题名称等方式进行多头申报的；4.项目主要内容已由该单位单独或联合其他单位申报并已获得省科技计划立项的；5.项目未经主管部门组织推荐的。
（七） 主申报单位必须具备与申报项目内容相关的基础和能力，项目申报书的内容、指标必须明确客观，严禁各种夸大和作假行为。项目如获得立项要确保申报书和合同书的内容、指标的一致性，违者按有关规定严肃处理。
三、申报程序
（一） 注册。首次申报的单位可在省专项资金管理统一平台进行注册后转入省科技业务管理阳光政务平台进行申报；或者在省科技业务管理阳光政务平台注册单位信息，获得单位用户名和密码，同时获得为本单位项目申报人开设用户帐号的权限，项目主持人从单位科研管理人员处获得用户名和密码，填写个人信息后进行申报。已注册的单位继续使用原有帐号进行申报和管理。
（二） 申报。各单位和申报人注册后即可通过网络提交申请书及相关材料，并在主管部门审核通过后打印书面申报书一式1份（含通过系统上传的所有附件和真实性承诺函原件）送交所属主管部门，由主管部门汇总审核后统一交科技厅业务受理窗口。
（三） 审核推荐。各级主管部门在省科技业务管理阳光政务平台对申报项目择优推荐，并正式行文（含推荐项目汇总表）分别报送省科技厅、省财政厅。其中各地级以上市所属企事业单位的申报项目，必须由地级以上市科技局和财政局联合行文报送；其余省直等相关部门所属企事业单位的申报项目，由主管部门行文报送。
（四） 资格审查和项目评审。省科技厅会同省财政厅组织专家，对各主管部门推荐的项目进行资格审查和评审，按照竞争择优的原则列入科技计划予以支持。
四、申报时间
申报单位网上申报及提交截止时间为2015年3月20日下午5:00时，各级科技主管部门网上审核推荐截止时间为2015年3月27 日下午5:00时。书面申报材料送省科技厅业务受理窗口的截止时间为2015年3月31日下午5:00时。
五、联系方式
书面材料报送地址：广州市连新路171号省科技信息大楼1楼广东省科技厅业务受理窗口（邮政编码：510033）
（一）省科技厅：
规划财务处 赵劲松，电话：020-83163832（综合性业务咨询）
广东省科技创新监测研究中心（技术支持）电话：020-83163338、83163469
各专题业务性问题请向申报指南所列联系人咨询。
（二） 省财政厅：
教科文处 廖建光，电话：020-83170070（财政资金管理服务）。

																				省科技厅 省财政厅
																				2015年2月16日
          </textarea>
        </div>
      </div>
      

      <div class="form-group">
        <div class="col-lg-11">
          <button class="btn btn-primary pull-right" type="button" onclick="cancel(1)">取消</button>
          <button class="btn btn-primary pull-right" type="button" onclick="save(1)" style="margin-right:1em;">保存信息</button>
          
        </div> 
      </div>

      <div id="attached" class="form-group" style="display:none">
        <!--
        <label for="" class="control-label col-xs-1"></label>
        <div class="col-xs-9">
          <input type="file" class="form-contol">
        </div>
        -->
        <div class="row" style="">
          <label for="" class="control-label col-xs-2"></label>
          <div class="col-xs-9">
            <input id="file-Portrait1" type="file" multiple>
          </div>
          <div class="col-lg-1" style="padding:0px; width:3%">
            <button class="btn btn-md" style="background-color:transparent; color:#2e6ca5">
              <i class="glyphicon glyphicon-plus"></i>
            </button>
        </div>
        </div>
      </div>

    </form>
  </section>
  
  
  
    
    <div id="abstract"></div>
    <section id="view"  class="abstractView" style="margin-top:1em">
    <div class="addAbstract">
        <span>&nbsp;</span>
    </div>
    <div class="subtitle">
        <h3 class="text-center">政策法规摘要信息表</h3>
    </div>
    
    <div class="clearfix"></div>
    <div class="main-info info" id="">
    </div>
    <div class="attention">
        <span>暂无信息</span>
    </div>
    </section>
   
  
  
    
    <section id="view"  class="fileView" style="margin-top:1em">
    <div class="subtitle" id="attachment">
        <h3 class="text-center">附件列表</h3>
    </div>
    <table class="table table-hover table-border table-striped">
      <tbody>
        <tr>
          <td style="font-weight:bold; text-align:center;" class="col-lg-1" >1</td>
          <td>关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》的通知</td>
          <td><span>32.1kb</span></button></td>
          <td><button class="download">下载</button></td>
          <td><button class="delete">删除</button></td>
        </tr>
        </tbody>
        </table>
    </section>
  
  <div id="root-footer"></div>
  <!---modal---->
  <div class="modal fade" id="addFaming" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">添加责任部门</h4>
        </div>
        <div class="modal-body">
          <form action="" class="">
            <div class="form-group">
              <label for="" class="label-control">责任部门：</label>
              <select name="" id="" class="selectpicker">
                <option value="">科技部门</option>
              </select>
            </div>
            <div class="form-group"><label for="" class="label-control">负责人：</label><input type="text" class="form-control"></div>
            <div class="form-group"><label for="" class="label-control">电话：</label><input type="text" class="form-control"></div>
            <div class="form-group"><label for="" class="label-control">传真：</label><input type="text" class="form-control"></div>
            <div class="form-group"><label for="" class="label-control">邮箱：</label><input type="text" class="form-control"></div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
          <button type="button" class="btn btn-primary">保存</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
</div>


<div class="container navbar" style="width:100%">
       <div class="row">
          <div class="col-xs-3">
            <img src="../images/logo-name-two.png" style="width:190px;padding-left:40px" alt="">
          </div>
          
          <div class="col-xs-3">
              <h4 style="font-weight:bold">·相关链接</h4>
              <ul style="margin:0;padding:0">
                  <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                  <br>
                  <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
              </ul>
          </div>
          <div class="col-xs-3">
              <h4 style="font-weight:bold">·相关链接</h4>
              <ul style="margin:0;padding:0">
                  <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                  <br>
                  <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                 
              </ul>
          </div>
          <div class="col-xs-3">
              <h4 style="font-weight:bold">·相关链接</h4>
              <ul style="margin:0;padding:0">
                  <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                  <br>
                  <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                  
              </ul>
          </div>
      </div>
</div>


<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/modernizr/modernizr.js"></script>
<script type="text/javascript" src="../js/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../js/app/main.js"></script>
<script type="text/javascript" src="../js/app/myjs.js"></script>

<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/affix.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/popover.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/carousel.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tab.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/select.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='https:\\www.google-analytics.com\analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

<script type="text/javascript">
  $('.selectpicker').selectpicker();
  $("#navigate li").click(function(){
	  $(this).siblings().removeClass("active");
	  $(this).addClass("active");
  })
  
  function modify(choose){
	  if(choose == 1){
		  $("section.info").hide();
		  $("section.infoForm").show();
		  window.location = "#";
	  }
	  if(choose == 2){
		  $("section.abstractView").hide();
		  $("section.abstractForm").show();
		  window.location = "#infoModify";
	  }
  }
  
  function save(choose){
	  if(choose == 1){
		  $("section.info").show();
		  $("section.infoForm").hide();
		  window.location = "#";
	  }
	  if(choose == 2){
		  $("section.abstractView").show();
		  $("section.abstractForm").hide();
		  window.location = "#infoModify";
	  }
  }
  
  function cancel(choose){
	  if(choose == 1){
		  $("section.info").show();
		  $("section.infoForm").hide();
		  window.location = "#";
	  }
	  if(choose == 2){
		  $("section.abstractView").show();
		  $("section.abstractForm").hide();
		  window.location = "#infoModify";
	  }
  }
  
  $(".addAbstract").click(function(){
	  window.location = "./poAddAbstract";
  })
</script>

</body>

</html>