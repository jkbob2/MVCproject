<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
<title>政策管理-部门总览</title>
<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->

<!-- build:css styles\vendor.css -->
<!-- bower:css -->
<link rel="stylesheet" href="../js/bootstrap-select/dist/css/bootstrap-select.min.css?version=20160108">
<link rel="stylesheet" href="../js/bootstrap/dist/css/bootstrap.min.css?version=20160108">
<!-- endbower -->
<!-- endbuild -->

<!-- build:css styles\main.css -->
<link rel="stylesheet" href="../css/app/main.css?version=20160108">
<link rel="stylesheet" href="../css/app/mycss.css?version=20160108">

</head>
<body>

<!--[if lt IE 10]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http:\\browsehappy.com\">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header>
  <nav class="navbar navbar-fixed-top navbar-default gradualChange" style="background-color:transparent">
   <div class="container-fluid" style="background-color:transparent">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="navbar-header" style="background-color:transparent">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="#">band</a>
     </div>

     <!-- Collect the nav links, forms, and other content for toggling -->
     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
       <ul class="nav navbar-nav navbar-right">
         <li class="active"><a href="#"style="background-color:transparent">首页 <span class="sr-only">(current)</span></a></li>
         <li><a href="#" style="background-color:transparent">企业管理</a></li>
         <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">高校研究院 <span class="caret"></span></a>
           <ul class="dropdown-menu">
             <li><a href="#">高校</a></li>
             <li><a href="#">科研院所</a></li>
           </ul>
         </li>
         <li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">政策管理 <span class="caret"></span></a>
           <ul class="dropdown-menu">
             <li><a href="poSearchInstitution">省级行政单位</a></li>
             <li><a href="poSearchInstitution">市级行政单位</a></li>
             <li><a href="poSearchInstitution">区级行政单位</a></li>
           </ul>
         </li>

       </ul>
     </div><!-- \.navbar-collapse -->
   </div><!-- \.container-fluid -->
  </nav>
</header>

<div id="context" class="container" style="width:90%">
<div class="row">
 
 <div class="col-lg-2 bs-docs-sidebar">             <!--侧边导航-->
   <ul class="nav nav-list bs-docs-sidenav affix" id="navigate">
     <li class="active"><a href="#">政策搜索</a></li>
     <li><a href="#sSearch-box"><i class="glyphicon glyphicon--chevron-right"></i> 高级搜索</a></li>
     <li><a href="#list4ins"><i class="glyphicon glyphicon--chevron-right"></i> 机构列表</a></li>
     <li><a href="#districtlevel"><i class="glyphicon glyphicon--chevron-right"></i> 区级机构</a></li>
   </ul>  
 </div>
 
 <div class="col-lg-10" id="container4context" style="margin-top:-20px">
   <section id="search-box" class="row" style="margin-top:2em">
     <div class="col-lg-8">
     <select class="selectpicker" data-width="auto">
       <option>中国</option>
     </select>
     <select class="selectpicker" data-width="auto">
       <option>广东省</option>
     </select>
     <select class="selectpicker" data-width="auto">
       <option>全部</option>
       <option>广州市</option>
     </select>
     <select class="selectpicker" data-width="auto">
       <option>全部</option>>
       <option>天河区</option>
       <option>番禺区</option>
       <option>越秀区</option>
       <option>黄浦区</option>
       <option>白云区</option>
     </select>
     <select class="selectpicker" data-width="auto">
       <option>科学技术厅</option>
     </select>
     <select class="selectpicker" data-width="auto">
       <option>规划财务处</option>
     </select>
     </div>
     <div class="col-lg-3 ">
     <div class="input-group">
       <input type="text" class="form-control" placeholder="输入关键字搜索">
       <span class="input-group-btn">
         <button class="btn btn-default" type="button">搜索</button>
       </span>
     </div><!-- \input-group -->
     </div><!-- \.col-lg-6 -->      <!--检索搜索-->
     <div class="col-lg-1 pull-right" style="padding:0px;">
       <button class="btn btn-primary" type="button" id="seniorSearch">高级检索</button>
     </div><!-- \.col-lg-6 -->      <!--检索搜索-->
   </section>
   <section id="sSearch-box"  value="0">
     <div id="view" class="row" style="margin-left:-15px; margin-bottom:15px; height:60px">
       <div class="title">
         <h3 class="text-center">高级检索模式</h3>
       </div>
     </div>
       
     <div class="row" style="margin-left:15px;">
       <div class="col-lg-3 info-title" style="height: 30px;margin-left:0px;border-radius: 2px; border-color:#6699cc">
         <h4 style="margin-top:0px">输入内容检索条件</h4>
       </div>            
     </div>
     <div class="row" style="padding-left:15px; margin-top:15px;">
       <div class="col-lg-1">
         <select class="selectpicker" data-width="auto">
           <option>政策名称</option>
           <option>印发部门</option>
           <option>行政区域</option>
           <option>关键字</option>
           <option>摘要</option>
           <option>内容</option>
           <option>全文</option>
         </select>
       </div>
       <div class="col-lg-3">
         <input type="text" class="form-control " placeholder="输入搜索词" style="margin-left:35px" />
       </div>
       <div class="col-lg-1" style="margin-left:20px">
         <select class="selectpicker" data-width="auto">
           <option>词频</option>
           <option>2</option>
           <option>3</option>
           <option>4</option>
           <option>5</option>
           <option>6</option>
           <option>7</option>
           <option>8</option>
           <option>9</option>
         </select>
       </div>
       <div class="col-lg-1">
         <select class="selectpicker" data-width="auto">
           <option>并含</option>
           <option>或含</option>
           <option>不含</option>
         </select>
       </div>
       <div class="col-lg-3">
         <input type="text" class="form-control col-lg-3" placeholder="输入搜索词"/>
       </div>
       <div class="col-lg-1" style="padding-left:0px;">
         <select class="selectpicker" data-width="auto">
           <option>词频</option>
           <option>2</option>
           <option>3</option>
           <option>4</option>
           <option>5</option>
           <option>6</option>
           <option>7</option>
           <option>8</option>
           <option>9</option>
         </select>
       </div>
       <div class="col-lg-1" style="padding-left:0px;">
         <select class="selectpicker" data-width="auto">
           <option>精确</option>
           <option>模糊</option>
         </select>
       </div>
       <div class="col-lg-1" style="padding:0px; width:3%">
         <button class="btn btn-md" style="background-color:transparent; color:#2e6ca5">
           <i class="glyphicon glyphicon-plus"></i>
         </button>
       </div>
     </div>

     <div class="row" style="padding-left:15px; margin-top:15px;">
       <div class="col-lg-1">
         <select class="selectpicker" data-width="auto">
           <option>政策名称</option>
           <option>印发部门</option>
           <option>行政区域</option>
           <option>关键字</option>
           <option>摘要</option>
           <option>内容</option>
           <option>全文</option>
         </select>
       </div>
       <div class="col-lg-3">
         <input type="text" class="form-control col-lg-3" placeholder="输入搜索词" style="margin-left:35px" />
       </div>
       <div class="col-lg-1" style="margin-left:20px">
         <select class="selectpicker" data-width="auto">
           <option>词频</option>
           <option>2</option>
           <option>3</option>
           <option>4</option>
           <option>5</option>
           <option>6</option>
           <option>7</option>
           <option>8</option>
           <option>9</option>
         </select>
       </div>
       <div class="col-lg-1">
         <select class="selectpicker" data-width="auto">
           <option>并含</option>
           <option>或含</option>
           <option>不含</option>
         </select>
       </div>
       <div class="col-lg-3">
         <input type="text" class="form-control col-lg-3" placeholder="输入搜索词"/>
       </div>
       <div class="col-lg-1" style="padding-left:0px;">
         <select class="selectpicker" data-width="auto">
           <option>词频</option>
           <option>2</option>
           <option>3</option>
           <option>4</option>
           <option>5</option>
           <option>6</option>
           <option>7</option>
           <option>8</option>
           <option>9</option>
         </select>
       </div>
       <div class="col-lg-1" style="padding-left:0px;">
         <select class="selectpicker" data-width="auto">
           <option>精确</option>
           <option>模糊</option>
         </select>
       </div>
       <div class="col-lg-1" style="padding:0px; width:3%">
         <button class="btn btn-md" style="background-color:transparent; color:#dd6666">
           <i class="glyphicon glyphicon-minus"></i>
         </button>
       </div>
     </div>

     <div class="row" style="margin:15px; margin-top:30px;">
       <div class="col-lg-3 info-title" style="height: 30px;margin-left:0px;border-radius: 2px; border-color:#6699cc">
         <h4 style="margin-top:0px">输入检索控制条件</h4>
       </div>            
     </div>
     <div class="row">
       <div class="col-lg-1"></div>
       <div class="col-lg-6">
         <div class="row">
           <h4 class="col-lg-4 text-center">印发时间：</h4>
           <h4 class="col-lg-1 text-center">从</h4>
           <input style="margin-top:6px" class="col-lg-3 input-append date form_datetime" id="datetimepicker" size="10" type="text" value="" data-date-format="yyyy-mm-dd">
           <h4 class="col-lg-1 text-center">到</h4>
           <input style="margin-top:6px" class="col-lg-3 input-append date form_datetime" id="datetimepicker" size="10" type="text" value="" data-date-format="yyyy-mm-dd">
         </div>
       </div>  
     </div>
     <div class="row">
       <div class="col-lg-1"></div>
       <div class="col-lg-6">
         <div class="row">
           <h4 class="col-lg-4 text-center">有效时间：</h4>
           <h4 class="col-lg-1 text-center">从</h4>
           <input style="margin-top:6px" class="col-lg-3 input-append date form_datetime" id="datetimepicker" size="10" type="text" value="" data-date-format="yyyy-mm-dd">
           <h4 class="col-lg-1 text-center">到</h4>
           <input style="margin-top:6px" class="col-lg-3 input-append date form_datetime" id="datetimepicker" size="10" type="text" value="" data-date-format="yyyy-mm-dd">
         </div>
       </div>  
     </div>
     <div class="row">
       <div class="col-lg-1"></div>
       <div class="col-lg-6">
         <div class="row">
           <h4 class="col-lg-4 text-center">印发部门：</h4>
           <div class="col-lg-8" style="padding:0px">
             <input type="text" class="form-control" placeholder="输入发布机构搜索" />
           </div>
         </div>
       </div>  
     </div>

     <div class="row">           <!-- 检索模式 -->
       <input-group style="margin:15px;" class="col">
         <div class="row" style="margin-left:15px;margin-top:15px;">    <!--第一行-->
           <h4 class="col-lg-2" style="margin:0px; padding:0px; text-align:center;">检索技巧：</h4>
           <label class="col-lg-2"><input type="checkbox" >前缀检索</label>
           <label class="col-lg-2"><input type="checkbox">短语检索</label>
           <label class="col-lg-2"><input type="checkbox" >模糊检索</label>
           <label class="col-lg-4"><input type="checkbox" >多关键字检索</label>
           <label class="col-lg-2"></label>
           <label class="col-lg-2"><input type="checkbox" >正则表达式检索</label>
           <label class="col-lg-2"><input type="checkbox" >通配符检索</label>
         </div>
         <div class="row" style="margin-left:15px;margin-top:15px;">          <!--第三行-->
           <h4 class="col-lg-2" style="margin:0px; padding:0px; text-align:center;">其他选项：</h4>
           <label class="col-lg-2"><input type="checkbox" checked="checked" >检索已过期文件</label>
           <label class="col-lg-2"><input type="checkbox" checked="checked" >检索范围边界</label>
         </div>
         <div class="row" style="margin-left:15px;">          <!--第三行-->
           <h4 class="col-lg-2" style="margin:0px; padding:0px; text-align:center;">结果排序：</h4>
           <label class="col-lg-2"><input name="sort" type="radio" />按匹配度排序</label>
           <label class="col-lg-2"><input name="sort" type="radio" />按相似度排序</label>
         </div>
       </input-group>
     </div>
     <div class="row">
       <div class="col-lg-6 ">
         <button class="btn btn-primary pull-right" type="button">开始检索</button>
       </div>
       <div class="col-lg-6">
         <button class="btn btn-default" type="button">结果中检索</button>
       </div>   
     </div>
     <div class="row" style="margin-left:15px;">
       <div class="col-lg-3 info-title" style="height: 30px;margin-left:0px;border-radius: 2px; border-color:#6699cc;margin-top:20px">
         <h4 style="">搜索结果</h4>
       </div>            
     </div>
     <div class="row" style="height:20px;"> </div>
     <table class="table table-hover table-border table-striped">
     <thead class="sort4table">
       <th>印发部门</th>
       <th>政策标题</th>
       <th>发布时间</th>
       <th>有效时间</th>
       <th>文号</th> 
     </thead>
     <tbody>
       <tr>
         <td>广东省科学技术厅规划财务处</td>
         <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
         <td>2015-01-01</td>
         <td>2015-01-02</td>
         <td>196号</td>
       </tr>
       <tr>
         <td>广东省科学技术厅规划财务处</td>
         <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
         <td>2015-01-02</td>
         <td>2015-01-04</td>
         <td>192号</td>
       </tr>
       <tr>
         <td>广东省科学技术厅规划财务处</td>
         <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
         <td>2015-01-04</td>
         <td>2015-01-03</td>
         <td>193号</td>
       </tr>
       <tr>
         <td>广东省科学技术厅规划财务处</td>
         <td><a href="">关于印发《2015年广东省协同创新与平台环境建设专项资金申报指南》</a></td>
         <td>2015-01-05</td>
         <td>2015-01-05</td>
         <td>190号</td>
       </tr>
     </tbody>
   </table>
   </section>

   <section  id="view" style="margin-top:1em" class="row">
     <div id="list4ins" class="title">
       <h3 class="text-center">中华人民共和国政府部门总览表</h3>
     </div>
     
     <div class="main-info info content" >
       <div class="title-small" id="countrylevel">
         <h4 class="text-center col">国家级政府机构</h4>
       </div>         
       <div class="row">                            <!--first row-->
         <div class="con col-lg-9 contitle">
           <a href="#">中央人民政府</a>
         </div>
         
         <div class="btn-group">
           <span class="btn btn-default btn-sm" href="#">展开 
             <i class="glyphicon glyphicon-chevron-down"></i>
           </span>
         </div>

         
       </div>
     
       <div class="row hiddenDiv">                             <!--second row-->
         <div class="con col-lg-4">
           <a href="#">国务院</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-4">
           <a href="#">发展改革委员会</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-4">
           <a href="#">科技部</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-4">
           <a href="#">工业和信息化部</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-4">
           <a href="#">财政部</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-4">
           <a href="#">知识产权局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-4">
           <a href="#">版权局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-4">
           <a href="#">农业部</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-4">
           <a href="#">商务部</a>
           <div class="clearfix"></div>
         </div>
       </div>

     </div>
   </section>

   <section id="inst-list">
     <div class="title-small" id="provincelevel">
         <h4 class="text-center col">省级政府机构</h4>
     </div>
     <div class="main-info info content" >
       <div class="row">
         <div class="col-lg-3 contitle">
           <a href="#">广东省人民政府</a>
         </div>
       </div>
       <div class="row">                            <!--second row-->
         <div class="con col-lg-3">
           <a href="#">广东省人民政府办公厅</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广东省发展和改革委员会</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-6">
           <a href="#">广东省经济和信息化委员会</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广东省科学技术厅</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广东省商务厅</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-6">
           <a href="#">广东省知识产权局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广东省质量技术监督局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广东省农业厅</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-6">
           <a href="#">广东省财政厅</a>
           <div class="clearfix"></div>
         </div>
       
       </div>
     </div>
   </section>

   <section id="inst-list">
     <div class="title-small" id="citylevel">
         <h4 class="text-center col">市级政府机构</h4>
     </div>
     <div class="main-info info content" >
       <div class="row">
         <div class="col-lg-5 contitle ">
           <a href="#">广州市人民政府</a>
         </div>
         <div class="col-lg-7">
           <select class="selectpicker" data-width="auto">
             <option>广东省</option>
             <option>广西省</option>
             <option>云南省</option>
             <option>江苏省</option>
           </select>
         </div>
       </div>
       <div class="row">                            <!--second row-->
         <div class="con col-lg-3">
           <a href="#">广州市人民政府办公厅</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市发展和改革委员会</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-6">
           <a href="#">广州市工业和信息化委员会</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市科技创新委员会</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市知识产权局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-6">
           <a href="#">广州市质量技术监督局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市农业局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市财政局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-6">
           <a href="#">广州市商务委员会</a>
           <div class="clearfix"></div>
         </div>
       
       </div>
     </div>
   </section>

   <section id="inst-list">
     <div class="title-small" id="districtlevel">
         <h4 class="text-center col">区级政府机构</h4>
     </div>
     <div class="main-info info content" >
       <div class="row">
         <div class="contitle col-lg-3">
           <a href="#">广州市人民政府</a>
         </div>
       </div>
       <div class="row">                            <!--second row-->
         <div class="con col-lg-3">
           <a href="#">广州市人民政府办公厅</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市发展和改革委员会</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-6">
           <a href="#">广州市工业和信息化委员会</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市科技创新委员会</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市知识产权局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-6">
           <a href="#">广州市质量技术监督局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市农业局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-3">
           <a href="#">广州市财政局</a>
           <div class="clearfix"></div>
         </div>
         <div class="con col-lg-6">
           <a href="#">广州市商务委员会</a>
           <div class="clearfix"></div>
         </div>
       
       </div>
     </div>
   </section>




   <section>               <!--换页索引-->
      <nav class="pull-right">
        <ul class="pagination">
          <li>
            <a href="#" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>
          <li><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li>
            <a href="#" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
            </a>
          </li>
        </ul>
      </nav>
    </section>
  </div> 

 </div>
</div>

<div class="container navbar" style="width:100%">
        <div class="row">
           <div class="col-xs-3">
             <img src="../images/logo-name-two.png" style="width:190px;padding-left:40px" alt="">
           </div>
           
           <div class="col-xs-3">
               <h4 style="font-weight:bold">·相关链接</h4>
               <ul style="margin:0;padding:0">
                   <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                   <br>
                   <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
               </ul>
           </div>
           <div class="col-xs-3">
               <h4 style="font-weight:bold">·相关链接</h4>
               <ul style="margin:0;padding:0">
                   <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                   <br>
                   <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                  
               </ul>
           </div>
           <div class="col-xs-3">
               <h4 style="font-weight:bold">·相关链接</h4>
               <ul style="margin:0;padding:0">
                   <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                   <br>
                   <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                   
               </ul>
           </div>
       </div>
 </div>


<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/modernizr/modernizr.js"></script>
<script type="text/javascript" src="../js/bootstrap-select/dist/js/bootstrap-select.js"></script>
<script type="text/javascript" src="../js/app/main.js"></script>
<script type="text/javascript" src="../js/app/myjs.js"></script>

<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/affix.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/popover.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/carousel.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tab.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/select.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='https:\\www.google-analytics.com\analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

<script type="text/javascript">
  $('.selectpicker').selectpicker();
  $("div.contitle a,div.con a").click(function(){
	  var institutionData = {
			  name:$(this).text(),
	  }
	  window.location = "./poViewList";
  });
  
  $('#sSearch-box').hide();
  $('#seniorSearch').click(function(){
    if($('#sSearch-box').attr("value")==0){
      $("#sSearch-box").attr("value","1");
      $('#sSearch-box').show();
    }
    else{
      $("#sSearch-box").attr("value","0");
      $('#sSearch-box').hide();
    }
  
  });
  
  $('.sort4table').click(function(){
	    sort();
  });
  
  $("#navigate li").click(function(){
	  $(this).siblings().removeClass("active");
	  $(this).addClass("active");
  })
  
  function initPages(){
		  
  }
  
  $(function(){
	   initPages();
  });
</script>

</body>

</html>