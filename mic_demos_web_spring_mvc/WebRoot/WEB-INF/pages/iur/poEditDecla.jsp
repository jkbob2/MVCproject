<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" /> 
<title>基本信息</title>
<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->

<!-- build:css styles\vendor.css -->
<!-- bower:css -->
<link rel="stylesheet" href="../js/bootstrap-select/dist/css/bootstrap-select.min.css?version=20160108">
<link rel="stylesheet" href="../js/bootstrap/dist/css/bootstrap.min.css?version=20160108">
<!-- endbower -->
<!-- endbuild -->

<!-- build:css styles\main.css -->
<link rel="stylesheet" href="../css/app/main.css?version=20160108">
<link rel="stylesheet" href="../css/app/mycss.css?version=20160108">

</head>
<body>

<!--[if lt IE 10]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http:\\browsehappy.com\">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header>
  <nav class="navbar navbar-fixed-top navbar-default " style="background-color:transparent">
    <div id="nav_sec" class="container-fluid gradualChange" style="background-color:transparent">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header" style="background-color:transparent">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Brand</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="#"style="background-color:transparent">首页 <span class="sr-only">(current)</span></a></li>
          <li><a href="#" style="background-color:transparent">企业管理</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">高校研究院 <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">高校</a></li>
              <li><a href="#">科研院所</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="background-color:transparent">政策管理 <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="poSearchInstitution.html">省级行政单位</a></li>
              <li><a href="poSearchInstitution.html">市级行政单位</a></li>
              <li><a href="poSearchInstitution.html">区级行政单位</a></li>
            </ul>
          </li>

        </ul>
      </div><!-- \.navbar-collapse -->
    </div><!-- \.container-fluid -->
    <div class="container" style="text-align:center;width:90%;">
      <div class="row">
      <div class="col-lg-10 col-lg-offset-2">
      <div style="margin:0 -15px;" id="nav_id"></div>
      </div>
      </div>
  </nav>
</header>


<div id="context" class="container" style="width:90%">

  <div class="row">

    <div class="col-lg-2 bs-docs-sidebar">
      <ul class="nav nav-list bs-docs-sidenav affix">
        <li><a href="#bs-example-navbar-collapse-1"><i class="icon-chevron-right"></i> 基本信息</a></li>
        <li class="active"><a href="#view"><i class="icon-chevron-right"></i> 申报信息</a></li>
        <li><a href="#table"><i class="icon-chevron-right"></i> 相关链接</a></li>
        <li><a href="#forms"><i class="icon-chevron-right"></i> ###</a></li>
        <li><a href="#buttons"><i class="icon-chevron-right"></i> ###</a></li>
      </ul>
    </div>
  
  <div class="col-lg-10" id="container4context">

  <section id="view" style="margin-top:1em" class="row" >
  <div id="name" class="title">
        <h3 class="text-center">政策申报信息</h3>
      </div>
      </section>
    
  <section id="form" style="margin-top:20px">
    <form action="" class="form-horizontal">
      <div class="form-group">
        <label for="" class="control-label col-xs-2">申报对象</label>
        <div class="col-xs-9"><input type="text" class="form-control"></div>
      </div>
      <div class="form-group">
        <label for="" class="control-label col-xs-2">提交网站</label>
        <div class="col-xs-9">
          <input type="text" class="form-control">
          <p class="help-block">电子版材料提交网址</p>
        </div>
        </div>

        <div class="form-group">
        <label for="" class="control-label col-xs-2">提交时间</label>
        <div class="col-xs-9">
          <input type="text" value="2012-05-15 21:05" class="form-control" id="">
        </div>
      </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-2">提交地点</label>
        <div class="col-xs-9">
          <input type="text" class="form-control">
          <p class="help-block">纸质版材料提交地点</p>
        </div>
        </div>

        <div class="form-group">
        <label for="" class="control-label col-xs-2">提交时间</label>
        <div class="col-xs-9">
          <input type="text" value="2012-05-15 21:05" class="form-control" id="">
        </div>
      </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-2">申报条件</label>
        <div class="col-xs-9">
          <textarea id="editor" class="editor">
            
          </textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="" class="control-label col-xs-2">申报材料</label>
        <div class="col-xs-9">
          <textarea class="editor"></textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="" class="control-label col-xs-2">原文链接</label>
        <div class="col-xs-9"><input type="text" class="form-control"></div>
      </div>

      <div class="form-group">
        <label for="" class="control-label col-xs-1"></label>
        <div class="col-xs-9">
          <input type="file" class="form-contol">
        </div>
      </div>

      <!--<div class="form-group">
            <label for="" class="control-label col-sm-1">附件</label>
            <div class="col-sm-11">
              <div class="fileinput fileinput-new" data-provides="fileinput">
<div class="fileinput fileinput-new input-group" data-provides="fileinput">
  <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
  <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
  <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
</div>
            </div>
          </div>
          </div>-->
    </form>
  </section>
  <div id="root-footer"></div>
</div>
<!--<footer>
  <p class="center">
    
    <a href="#">联系我们</a>
    <span>|</span>
    <a href="#">常见问题</a>
    <span>|</span>
    <a href="#">成功案例</a>
    <span>|</span>
  </p>
  <p>广州粤立企业顾问有限公司</p>
</footer>-->
<br> 
  <div class="container navbar" style="width:100%">
         <div class="row">
            <div class="col-xs-3">
              <img src="../images/logo-name-two.png" style="width:190px;padding-left:40px" alt="">
            </div>
            
            <div class="col-xs-3">
                <h4 style="font-weight:bold">·相关链接</h4>
                <ul style="margin:0;padding:0">
                    <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                    <br>
                    <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                </ul>
            </div>
            <div class="col-xs-3">
                <h4 style="font-weight:bold">·相关链接</h4>
                <ul style="margin:0;padding:0">
                    <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                    <br>
                    <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                   
                </ul>
            </div>
            <div class="col-xs-3">
                <h4 style="font-weight:bold">·相关链接</h4>
                <ul style="margin:0;padding:0">
                    <a href="#" style="font-size:6px;">广东省阳光政务平台1</a>
                    <br>
                    <a href="#" style="font-size:6px;">广东省阳光政务平台2</a>
                    
                </ul>
            </div>
        </div>
  </div>


<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/modernizr/modernizr.js"></script>
<script type="text/javascript" src="../js/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../js/app/main.js"></script>
<script type="text/javascript" src="../js/app/myjs.js"></script>

<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/affix.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/alert.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/transition.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/button.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/popover.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/carousel.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/collapse.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/tab.js"></script>
<script type="text/javascript" src="../js/bootstrap-sass/assets/javascripts/bootstrap/select.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
  function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
  e=o.createElement(i);r=o.getElementsByTagName(i)[0];
  e.src='https:\\www.google-analytics.com\analytics.js';
  r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

<script type="text/javascript">
  $('.selectpicker').selectpicker();
</script>

</body>

</html>