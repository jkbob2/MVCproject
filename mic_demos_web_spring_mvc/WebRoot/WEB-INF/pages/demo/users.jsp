<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Hello World</title>
</head>
<body>
	<h2>您好, ${showname}</h2>
	&nbsp;&nbsp;&nbsp;
	<a href="<c:url value="/index.jsp" />">首页</a> &nbsp;&nbsp;&nbsp;
	<a href="<c:url value="/opr/listDevices" />">系统设备</a> &nbsp;&nbsp;&nbsp;
	<a href="<c:url value="/logout" />">退出</a>

	<br>
	<h3>从数据库 db1 获取的用户数据</h3>
	<table border="1">
		<tr>
			<th>登录名</th>
			<th>显示名</th>
			<th>角色</th>
		</tr>
		<c:forEach var="user" items="${users}">
			<tr>
				<td>${user.username}</td>
				<td>${user.showname}</td>
				<td>${user.rolecode}</td>
			</tr>
		</c:forEach>
	</table>

</body>
</html>