<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Hello World</title>
</head>
<body>
	<h2>您好, ${showname}</h2>
	&nbsp;&nbsp;&nbsp;
	<a href="<c:url value="/index.jsp" />">首页</a> &nbsp;&nbsp;&nbsp;
	<a href="<c:url value="/admin/listUsers" />">系统用户</a> &nbsp;&nbsp;&nbsp;
	<a href="<c:url value="/logout" />">退出</a>

	<br>
	<h3>从数据库 db2 获取的设备数据</h3>
	<table border="1">
		<tr>
			<th>仪表名称</th>
			<th>类型</th>
		</tr>
		<c:forEach var="device" items="${devices}">
			<tr>
				<td>${device.devicename}</td>
				<td>${device.type}</td>
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>