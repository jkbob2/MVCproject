package com.mic.demo.test.dao;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.mic.demo.core.dao.UserDAO_Object;
import com.mic.demo.core.dao.UserDAO_Object_JDBC_Impl;
import com.mic.demo.core.model.User;

import junit.framework.TestCase;

public class UserDAO_Object_Mybatis_Test extends TestCase {

	public static SqlSessionFactory sqlSessionFactory;

	static {
		// 实例化 SqlSessionFactory
		Reader reader;
		try {
			reader = Resources
					.getResourceAsReader("com/mic/demo/core/dao/mybatis.xml");
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void testUserDAO_Ojbect_Mybatis_listUsers() {
		
		SqlSession sqlSession = sqlSessionFactory.openSession();
		try {
			
			UserDAO_Object userDAO = sqlSession.getMapper(UserDAO_Object.class);
			
			List<User> users = userDAO.listUsers();
			Iterator iter = users.iterator();
			while (iter.hasNext()) {
				User user = (User) iter.next();
				System.out.println(user.toString());
			}
			
		}catch (Exception e) {

			e.printStackTrace();

		}
		sqlSession.close();
	}
	
public void testUserDAO_Ojbect_Mybatis_getUser() {
		
		SqlSession sqlSession = sqlSessionFactory.openSession();
		try {
			
			UserDAO_Object userDAO = sqlSession.getMapper(UserDAO_Object.class);
			
			userDAO = new UserDAO_Object_JDBC_Impl();
			User user = userDAO.getUser(5038);
			System.out.println(user.toString());
			
		}catch (Exception e) {

			e.printStackTrace();

		}
		sqlSession.close();
	}

}
