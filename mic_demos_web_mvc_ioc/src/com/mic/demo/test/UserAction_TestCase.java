package com.mic.demo.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mic.demo.web.action.Action;
import com.mic.demo.web.action.GetUserAction;
import com.mic.demo.web.ui.ModelAndView;

import junit.framework.Assert;
import junit.framework.TestCase;

public class UserAction_TestCase extends TestCase {
	
	private static ApplicationContext context;

	static {
		context = new ClassPathXmlApplicationContext(
				"classpath:configs/application-context-annotation.xml");
	}
	
	public void testGetUserAction_View_user(){
		GetUserAction action = (GetUserAction) context.getBean("getUserAction");
		action.setId(5038);
		
		ModelAndView mv  = action.execute();
		
		Assert.assertEquals(mv.getViewName(), "user");
		
		//System.out.println(mv.getViewName());
	}

}
