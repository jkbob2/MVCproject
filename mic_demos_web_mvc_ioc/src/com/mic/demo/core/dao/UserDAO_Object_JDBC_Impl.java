package com.mic.demo.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mic.demo.core.model.User;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import org.springframework.stereotype.Repository;

/**
 * 面向对象的用户数据访问接口 使用JDBC技术实现
 * 
 * 从代码可维护性角度，如果user增加性别属性呢？ 需要修改那些地方？
 * 
 * @author mulan
 * 
 */
@Repository("userDAOJDBC")
//Spring 注解，数据访问bean，运行时spring容器自动扫描该注解标签，将该类注册入spring bean容器
public class UserDAO_Object_JDBC_Impl extends JDBCSupport implements
		UserDAO_Object {

	public UserDAO_Object_JDBC_Impl() throws ClassNotFoundException {
		super();
		System.out.println("bean: userDAOJDBC being created!");

	}

	@Override
	public List<User> listUsers() {

		List<User> users = new ArrayList<User>();
		Connection conn = null;
		Statement stmt = null;
		try {
			// 从JDBCSupport中获取一个数据库链接
			conn = this.getConnection();
			stmt = (Statement) conn.createStatement();
			String sql = "select * from user";
			// 返回结果集，在读取结果集中的数据前，不能关闭数据库链接
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				// 创建User实例，并从结果集中获取数据，设置它的属性
				User user = new User();
				Integer id = rs.getInt("id");
				user.setId(id);
				String username = rs.getString("username");
				user.setUsername(username);
				String showname = rs.getString("showname");
				user.setShowname(showname);
				String password = rs.getString("password");
				user.setPassword(password);
				String question = rs.getString("question");
				user.setQuestion(question);
				String answer = rs.getString("answer");
				user.setAnswer(answer);
				String rolecode = rs.getString("rolecode");
				user.setRolecode(rolecode);
				Date lastlogintime = rs.getDate("lastlogintime");
				user.setLastlogintime(lastlogintime);
				boolean enabled = rs.getBoolean("enabled");
				user.setEnabled(enabled);
				boolean accountNonExpired = rs.getBoolean("accountNonExpired");
				user.setAccountNonExpired(accountNonExpired);
				boolean credentialsNonExpired = rs
						.getBoolean("credentialsNonExpired");
				user.setCredentialsNonExpired(credentialsNonExpired);
				boolean accountNonLocked = rs.getBoolean("accountNonLocked");
				user.setAccountNonLocked(accountNonLocked);

				users.add(user);
			}

		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}// nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}// end finally try
		}// end try

		return users;
	}

	@Override
	public User getUser(Integer id) {

		Connection conn = null;
		Statement stmt = null;
		try {
			conn = this.getConnection();
			stmt = (Statement) conn.createStatement();
			String sql = "select * from user where id=" + id;
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				// 创建User实例，并从结果集中获取数据，设置它的属性
				User user = new User();
				user.setId(id);
				String username = rs.getString("username");
				user.setUsername(username);
				String showname = rs.getString("showname");
				user.setShowname(showname);
				String password = rs.getString("password");
				user.setPassword(password);
				String question = rs.getString("question");
				user.setQuestion(question);
				String answer = rs.getString("answer");
				user.setAnswer(answer);
				String rolecode = rs.getString("rolecode");
				user.setRolecode(rolecode);
				Date lastlogintime = rs.getDate("lastlogintime");
				user.setLastlogintime(lastlogintime);
				boolean enabled = rs.getBoolean("enabled");
				user.setEnabled(enabled);
				boolean accountNonExpired = rs.getBoolean("accountNonExpired");
				user.setAccountNonExpired(accountNonExpired);
				boolean credentialsNonExpired = rs
						.getBoolean("credentialsNonExpired");
				user.setCredentialsNonExpired(credentialsNonExpired);
				boolean accountNonLocked = rs.getBoolean("accountNonLocked");
				user.setAccountNonLocked(accountNonLocked);

				return user;
			}

		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}// nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}// end finally try
		}// end try
		return null;
	}

}
