package com.mic.demo.web.servlet;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.mic.demo.web.action.Action;
import com.mic.demo.web.context.ActionHandler;
import com.mic.demo.web.context.ParametersHandler;
import com.mic.demo.web.ui.ModelAndView;

public class DispatcherServlet extends HttpServlet {

	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// 获取Spring Bean的上下文
		ServletContext sc = request.getServletContext();
		WebApplicationContext webContext = WebApplicationContextUtils
				.getWebApplicationContext(sc);

		// 得到当前Servlet的请求路径
		String servletPath = request.getServletPath(); // e.g. /listUsers
		String actionKey = StringUtils.replace(servletPath, "/", ""); // e.g.
																		// listUser
		// java 反射机制
		Class actionClass = ActionHandler.getActionHandler().getActionClass(
				actionKey);

		String dispatchPath = "/nonaction.jsp";

		try {
			// 从spring bean容器中获取相应的action
			Action action = (Action) webContext.getBean(actionClass);

			// 使用java反射机制，将request中的参数传给action中的相应属性
			// /getUser?id=5038, 和GetUserAction, 将5038设置到GetUserAction的id属性

			ParametersHandler parametersHandler = new ParametersHandler();
			parametersHandler.convert(request, action);

			ModelAndView mv = null;

			mv = action.execute();
			// 视图还未实现配置化
			if (mv.hasView()) {
				dispatchPath = "/WEB-INF/jsp/" + mv.getViewName() + ".jsp";
			}
			Map model = mv.getModel();
			request.setAttribute("model", model);

		} catch (Exception e) {
			e.printStackTrace();

		}
		RequestDispatcher rd = request.getRequestDispatcher(dispatchPath);
		rd.forward(request, response);

	}

}
