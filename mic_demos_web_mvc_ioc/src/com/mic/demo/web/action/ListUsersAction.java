package com.mic.demo.web.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import com.mic.demo.core.model.User;
import com.mic.demo.core.service.UserService;
import com.mic.demo.web.ui.ModelAndView;

@Controller("listUsersAction")
public class ListUsersAction implements Action {

	public ListUsersAction(){
		System.out.println("bean: listUsersAction being created!");
	}
	
	
	@Autowired
	@Qualifier("userService")
	private UserService userService;

	public ModelAndView execute() {

		ModelAndView mv = new ModelAndView();

		List<User> users = userService.listUsers();

		// 将model数据存放在request中，转发到jsp页面时，可从request中访问model数据
		// User 就是一个JavaBean－POJO类
		// 注意reqeust｜session｜application域的生命周期
		// request.setAttribute("users", users);

		// 需要缓存 users，在jsp等view视图中需要被访问
		mv.addObject("users", users);
		mv.setViewName("users");
		
		return mv;
	}

	// getters and setters
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
