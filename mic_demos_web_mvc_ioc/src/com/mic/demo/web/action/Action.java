package com.mic.demo.web.action;

import com.mic.demo.web.ui.ModelAndView;

public interface Action {
	public ModelAndView execute();
}
