package com.mic.demo.web.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import com.mic.demo.core.model.User;
import com.mic.demo.core.service.UserService;
import com.mic.demo.web.ui.ModelAndView;

@Controller("getUserAction")
// getUser?id=5038
public class GetUserAction implements Action {
	
	public GetUserAction(){
		System.out.println("bean: getUserAction being created!");
	}

	private Integer id;

	@Autowired
	@Qualifier("userService")
	private UserService userService;

	public ModelAndView execute() {
		
		ModelAndView mv = new ModelAndView();

		User user = null;
		try {
			user = userService.getUser(id);

			// 将model数据存放在request中，转发到jsp页面时，可从request中访问model数据
			// User 就是一个JavaBean－POJO类
			// 注意reqeust｜session｜application域的生命周期
			// request.setAttribute("user", user);
			mv.addObject("user", user);
		
		} catch (Exception e) {
		}
		mv.setViewName("user");
		return mv;

	}

	// getters and setters
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
