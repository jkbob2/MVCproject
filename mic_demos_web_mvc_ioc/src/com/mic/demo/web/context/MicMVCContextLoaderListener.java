package com.mic.demo.web.context;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MicMVCContextLoaderListener implements ServletContextListener{

	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		// TODO Auto-generated method stub
		ServletContext context = event.getServletContext();
		String actionMappingLocation = context.getInitParameter("actionMappingLocation");
		ActionHandler actionHandler = ActionHandler.getActionHandler();
		try {
			actionHandler.loadActionMapping(actionMappingLocation);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// TODO Auto-generated method stub
		
	}



}
