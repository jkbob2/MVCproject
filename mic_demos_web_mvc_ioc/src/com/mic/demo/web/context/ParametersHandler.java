package com.mic.demo.web.context;

import java.lang.reflect.Field;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.mic.demo.web.action.Action;

public class ParametersHandler {

	public void convert(HttpServletRequest request, Action action)
			throws Exception {

		// java 放射机制，获取action的所有属性
		Field[] fields = action.getClass().getDeclaredFields();
		// 获取request中的所有参数
		Enumeration enu = request.getParameterNames();
		while (enu.hasMoreElements()) {
			String key = (String) enu.nextElement();
			Field field = action.getClass().getDeclaredField(key);
			if (field != null) {
				field.setAccessible(true);
				String value = request.getParameter(key);
				String fieldType = field.getType().toString();
				if (fieldType.endsWith("String")) {
					field.set(action, value); // 给属性设值
				} else if (fieldType.endsWith("int")
						|| fieldType.endsWith("Integer")) {
					field.set(action, Integer.valueOf(value)); // 给属性设值
				} else {
					System.out.println("parameter is an unsupported type!");
				}
			}

		}

	}

}
