package com.mic.demo.core.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Spring JDBC数据库操作的封装类，实现数据库链接参数等的集中配置
 * 
 * 进行具体数据库操作的类可通过继承SpringJDBCSupport类获得数据库链接等配置
 * 
 * @author mulan
 * 
 */
public class SpringJDBCSupport {

	// javax.sql.DataSource
	private DataSource dataSource;

	// javax.sql.DataSource的一个具体实现类
	private DriverManagerDataSource driverManagerDataSource;

	// Spring JDBC对JDBC面向结果集操作接口的一个封装
	// 可使用ResultSet-Object Mapper转换类实现面向对象的接口
	private JdbcTemplate jdbcTemplateObject;

	private String driverClassName = "com.mysql.jdbc.Driver";

	private String url = "jdbc:mysql://127.0.0.1:3306/mic_demos?useUnicode=true&characterEncoding=utf8";

	private String username = "root";

	private String password = "ljw19960907";

	public SpringJDBCSupport() {

		driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName(driverClassName);
		driverManagerDataSource.setUrl(url);
		driverManagerDataSource.setUsername(username);
		driverManagerDataSource.setPassword(password);

		dataSource = driverManagerDataSource;
		jdbcTemplateObject = new JdbcTemplate(dataSource);

	}

	// getters and setters
	public DataSource getDataSource() {
		return dataSource;
	}

	public JdbcTemplate getJdbcTemplateObject() {
		return jdbcTemplateObject;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
