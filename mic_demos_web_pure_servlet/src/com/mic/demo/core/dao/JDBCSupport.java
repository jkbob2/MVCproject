package com.mic.demo.core.dao;

import java.sql.DriverManager;
import java.sql.SQLException;
import com.mysql.jdbc.Connection;

/**
 * JDBC数据库操作的封装类，实现数据库链接参数等的集中配置
 * 
 * 进行具体数据库操作的类可通过继承JDBCSupport类获得数据库链接等配置
 * @author mulan
 *
 */
public class JDBCSupport {

	private String driverClassName = "com.mysql.jdbc.Driver";

	private String url = "jdbc:mysql://127.0.0.1:3306/mic_demos?useUnicode=true&characterEncoding=utf8";

	private String username = "root";

	private String password = "ljw19960907";

	/**
	 * 
	 * @throws ClassNotFoundException
	 */
	public JDBCSupport() throws ClassNotFoundException {
		// 注册JDBC驱动
		Class.forName(driverClassName);

	}

	/**
	 * 打开一个新的数据链接
	 * 
	 * @return 数据库链接
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		// 打开数据库链接
		Connection connection = (Connection) DriverManager.getConnection(url, username, password);
		return connection;
	}

	// getters and setters

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
