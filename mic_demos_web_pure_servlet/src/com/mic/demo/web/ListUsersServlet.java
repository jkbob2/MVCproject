package com.mic.demo.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.mic.demo.core.model.User;
import com.mic.demo.core.service.UserService;

/**
 * Servlet: 在java技术平台中指运行在服务器端的一段程序，并通过一定的应用程层协议与客户端程序进行通信交互；
 * Servlet程序需要符合一定的规范，然后由支持该规范的容器提供运行时支持。
 * HttpServlet：使用http协议进行通信交互的Servlet，部署在web容器中运行，如Tomcat，Jboss等；
 * web容器管理HttpServlet程序的生命周期，并处理http协议的底层细节，
 * 为HttpServlet程序提供HttpServletRequest，HttpServletResponse和HttpSession等封装
 * 请查找资料，深入了解Servlet和HttpServlet等概念和原理
 * 
 * 
 * 单元测试：从测试的角度，一个模块与其他第三方模块的耦合程度越低，越容易实现独立单元测试。
 * 比如，UserDAO的JDBC实现类，较MyBatis的实现类容易测试，因为前者不涉及第三方模块（即Mybatis）的耦合；
 * MyBatis的实现类在测试时，需要设置好MyBatis运行时等。
 * 
 * GetUserServlet继承HttpServlet，与HttpServlet机制紧耦合；
 * 在单元测试时，如doGet函数时，需要传入HttpServletRequest和HttpServletResponse的实例；
 * 但是，一般单元测试过程缺少web容器，无法由web容器注入所需HttpServletRequest和HttpServletResponse实例；
 * 在实践中，一般需要由程序员编码构建伪（mock）HttpServletRequest和HttpServletResponse实例。
 * 
 * @author mulan
 *
 */
public class ListUsersServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {	
		
		ServletContext sc = request.getServletContext();
		WebApplicationContext webContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		UserService userService  = (UserService) webContext.getBean("userService");
		
		List<User> users = userService.listUsers();
		
		// 准备响应
		response.setContentType("text/html");  
       
		
		StringBuilder builder = new StringBuilder();
        builder.append("<html>");
        builder.append("<head><title>mic_demos_pure_servlet</title></head>");
        builder.append("<body>");   
        builder.append("<h3>系统用户列表</h3>");
        builder.append("<table border=\"1\">");
        builder.append("<tr><th>登录名</th><th>显示名</th><th>角色</th></tr>");
        
		Iterator iter = users.iterator();
		while (iter.hasNext()) {
			User user = (User) iter.next();
			
			builder.append("<tr>");
			builder.append("<td>");
			builder.append(user.getUsername());
			builder.append("</td>");
			builder.append("<td>");
			builder.append(user.getShowname());
			builder.append("</td>");
			builder.append("<td>");
			builder.append(user.getRolecode());
			builder.append("</td>");
			builder.append("</tr>");
		}
		builder.append("</table>");
        builder.append("</body>");
        builder.append("</html>");
       
        PrintWriter out = response.getWriter();
        out.write(builder.toString());
        response.flushBuffer();
        
	}

}
