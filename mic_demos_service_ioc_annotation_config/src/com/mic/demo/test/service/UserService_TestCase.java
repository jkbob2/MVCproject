package com.mic.demo.test.service;

import java.util.Iterator;
import java.util.List;

import com.mic.demo.core.model.User;
import com.mic.demo.core.service.UserService;
import com.mic.demo.core.service.UserServiceImpl;

import junit.framework.TestCase;

public class UserService_TestCase extends TestCase{
	
	
	public void testUserServiceImpl_listUsers(){
		try {
			UserService userService = new UserServiceImpl();
			List<User> users = userService.listUsers();
			Iterator iter = users.iterator();
			while (iter.hasNext()) {
				User user = (User) iter.next();
				System.out.println(user.toString());
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void testUserServiceImpl_getUser(){
		try {
			UserService userService = new UserServiceImpl();
			User user = userService.getUser(5038);
			System.out.println(user.toString());
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
