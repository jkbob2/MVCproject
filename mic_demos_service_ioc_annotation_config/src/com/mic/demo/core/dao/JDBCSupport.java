package com.mic.demo.core.dao;

import java.sql.DriverManager;
import java.sql.SQLException;
import com.mysql.jdbc.Connection;

/**
 * JDBC鏁版嵁搴撴搷浣滅殑灏佽绫伙紝瀹炵幇鏁版嵁搴撻摼鎺ュ弬鏁扮瓑鐨勯泦涓厤缃�
 * 
 * 杩涜鍏蜂綋鏁版嵁搴撴搷浣滅殑绫诲彲閫氳繃缁ф壙JDBCSupport绫昏幏寰楁暟鎹簱閾炬帴绛夐厤缃�
 * @author mulan
 *
 */
public class JDBCSupport {

	private String driverClassName = "com.mysql.jdbc.Driver";

	private String url = "jdbc:mysql://127.0.0.1:3306/mic_demos?useUnicode=true&characterEncoding=utf8";

	private String username = "root";

	private String password = "ljw19960907";

	/**
	 * 
	 * @throws ClassNotFoundException
	 */
	public JDBCSupport() throws ClassNotFoundException {
		// 娉ㄥ唽JDBC椹卞姩
		Class.forName(driverClassName);

	}

	/**
	 * 鎵撳紑涓�涓柊鐨勬暟鎹摼鎺�
	 * 
	 * @return 鏁版嵁搴撻摼鎺�
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		// 鎵撳紑鏁版嵁搴撻摼鎺�
		Connection connection = (Connection) DriverManager.getConnection(url, username, password);
		return connection;
	}

	// getters and setters

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
