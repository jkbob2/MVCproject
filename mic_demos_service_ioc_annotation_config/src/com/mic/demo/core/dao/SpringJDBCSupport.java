package com.mic.demo.core.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Spring JDBC鏁版嵁搴撴搷浣滅殑灏佽绫伙紝瀹炵幇鏁版嵁搴撻摼鎺ュ弬鏁扮瓑鐨勯泦涓厤缃�
 * 
 * 杩涜鍏蜂綋鏁版嵁搴撴搷浣滅殑绫诲彲閫氳繃缁ф壙SpringJDBCSupport绫昏幏寰楁暟鎹簱閾炬帴绛夐厤缃�
 * 
 * @author mulan
 * 
 */
public class SpringJDBCSupport {

	// javax.sql.DataSource
	private DataSource dataSource;

	// javax.sql.DataSource鐨勪竴涓叿浣撳疄鐜扮被
	private DriverManagerDataSource driverManagerDataSource;

	// Spring JDBC瀵笿DBC闈㈠悜缁撴灉闆嗘搷浣滄帴鍙ｇ殑涓�涓皝瑁�
	// 鍙娇鐢≧esultSet-Object Mapper杞崲绫诲疄鐜伴潰鍚戝璞＄殑鎺ュ彛
	private JdbcTemplate jdbcTemplateObject;

	private String driverClassName = "com.mysql.jdbc.Driver";

	private String url = "jdbc:mysql://127.0.0.1:3306/mic_demos?useUnicode=true&characterEncoding=utf8";

	private String username = "root";

	private String password = "ljw19960907";

	public SpringJDBCSupport() {

		driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName(driverClassName);
		driverManagerDataSource.setUrl(url);
		driverManagerDataSource.setUsername(username);
		driverManagerDataSource.setPassword(password);

		dataSource = driverManagerDataSource;
		jdbcTemplateObject = new JdbcTemplate(dataSource);

	}

	// getters and setters
	public DataSource getDataSource() {
		return dataSource;
	}

	public JdbcTemplate getJdbcTemplateObject() {
		return jdbcTemplateObject;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
