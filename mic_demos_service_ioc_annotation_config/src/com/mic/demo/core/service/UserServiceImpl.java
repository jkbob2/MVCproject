package com.mic.demo.core.service;

import java.util.List;

import com.mic.demo.core.dao.UserDAO_Object;
import com.mic.demo.core.dao.UserDAO_Object_JDBC_Impl;
import com.mic.demo.core.model.User;


/**
 * 程序员控制一个接口实现类的实例化，hardcoding，在运行时无法修改
 * 
 * 控制反转：将接口实现类注册容器中（如spring bean容器），
 * 由容器在运行时根据依赖关系创建实例，并管理该实例的生命周期；
 * 依赖注入：一个接口变量，在运行时由容器根据配置赋值（即一个该接口实现类的实例）
 * 见：getUser函数
 * 
 * @author mulan
 *
 */
public class UserServiceImpl implements UserService{

	private UserDAO_Object userDAO;
	
	public UserServiceImpl() throws ClassNotFoundException{
		// 程序员在编码时已经做出接口实现类的选择，并hardcoding在代码中
		// 系统编译，运行时时不能动态修改这个选择的
		// 如果系统在运行若干时间后，需要使用该接口类的另外一个实现呢？
		userDAO = new UserDAO_Object_JDBC_Impl();
	}
	
	@Override
	public List<User> listUsers() {
		// TODO Auto-generated method stub
		return userDAO.listUsers();
	}

	@Override
	public User getUser(Integer id) {
		// TODO Auto-generated method stub
		return userDAO.getUser(id);
	}

}
