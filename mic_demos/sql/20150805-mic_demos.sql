/*
 Navicat Premium Data Transfer

 Source Server         : mulan
 Source Server Type    : MySQL
 Source Server Version : 50617
 Source Host           : localhost
 Source Database       : mic_demos

 Target Server Type    : MySQL
 Target Server Version : 50617
 File Encoding         : utf-8

 Date: 08/05/2015 10:10:10 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `suggestion`
-- ----------------------------
DROP TABLE IF EXISTS `suggestion`;
CREATE TABLE `suggestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `userid` int(11) DEFAULT NULL COMMENT '提议人的id',
  `name` varchar(20) DEFAULT NULL COMMENT '提议人的姓名',
  `content` varchar(600) NOT NULL COMMENT '建议内容',
  `email` varchar(60) NOT NULL COMMENT '提议人邮箱地址',
  `isread` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已阅读；0：未阅读，1：已阅读',
  `issend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已发送邮件；0：未发送，1：已发送',
  `isnotice` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已反馈；0：未反馈，1：已反馈',
  PRIMARY KEY (`id`),
  KEY `fk_suggestion_user` (`userid`),
  CONSTRAINT `fk_suggestion_user` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(30) NOT NULL COMMENT '登录名',
  `showname` varchar(90) DEFAULT NULL COMMENT '显示名',
  `password` varchar(200) NOT NULL COMMENT '密码-SHA1哈希值',
  `question` varchar(45) DEFAULT NULL COMMENT '提问',
  `answer` varchar(45) DEFAULT NULL COMMENT '回答',
  `rolecode` varchar(100) NOT NULL COMMENT '用户类型{ROLE_ACAMGR|ROLE_TCH:|ROLE_STD}',
  `description` varchar(150) DEFAULT NULL COMMENT '备注',
  `lastlogintime` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用，“0-否”，“1-是，启用” 默认：1',
  `accountNonExpired` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否过期，“0-否”，“1-是，未过期” 默认：1',
  `credentialsNonExpired` tinyint(1) NOT NULL DEFAULT '1' COMMENT '密码是否失效，“0-否”，“1-是，未失效” 默认：1',
  `accountNonLocked` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否被锁定-“0-否”，“1-是，未被锁定” 默认：1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5041 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('5038', 'yan', '闫军威', '65e516bf851cdafdacd026500450e7b8a2e5f55d', '?', '远正', 'ROLE_ADMIN,ROLE_OPR,ROLE_PUB', null, null, '1', '1', '1', '1'), ('5039', 'zhang', '张胜强', '65e516bf851cdafdacd026500450e7b8a2e5f55d', '?', '远正智能', 'ROLE_OPR', null, null, '1', '1', '1', '1'), ('5040', 'chen', '陈春华', '65e516bf851cdafdacd026500450e7b8a2e5f55d', '?', '远正智能', 'ROLE_PUB', null, null, '1', '1', '1', '1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
