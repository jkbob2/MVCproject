/*
 Navicat Premium Data Transfer

 Source Server         : mulan
 Source Server Type    : MySQL
 Source Server Version : 50617
 Source Host           : localhost
 Source Database       : eva2014

 Target Server Type    : MySQL
 Target Server Version : 50617
 File Encoding         : utf-8

 Date: 08/04/2015 21:25:50 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `suggestion`
-- ----------------------------
DROP TABLE IF EXISTS `suggestion`;
CREATE TABLE `suggestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `userid` int(11) DEFAULT NULL COMMENT '提议人的id',
  `name` varchar(20) DEFAULT NULL COMMENT '提议人的姓名',
  `content` varchar(600) NOT NULL COMMENT '建议内容',
  `email` varchar(60) NOT NULL COMMENT '提议人邮箱地址',
  `isread` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已阅读；0：未阅读，1：已阅读',
  `issend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已发送邮件；0：未发送，1：已发送',
  `isnotice` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已反馈；0：未反馈，1：已反馈',
  PRIMARY KEY (`id`),
  KEY `fk_suggestion_user` (`userid`),
  CONSTRAINT `fk_suggestion_user` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
