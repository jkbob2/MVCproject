package com.mic.demo.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class UserDAO_ResultSet_JDBC_Impl extends JDBCSupport implements
		UserDAO_ResultSet {

	private Connection conn = null;
	private Statement stmt = null;

	public UserDAO_ResultSet_JDBC_Impl() throws ClassNotFoundException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public ResultSet listUsers() {

		try {
			conn = this.getConnection();
			stmt = (Statement) conn.createStatement();
			String sql = "select * from user";
			ResultSet rs = stmt.executeQuery(sql);
			return rs;
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}

		/*
		 * finally { // finally block used to close resources try { if (stmt !=
		 * null) stmt.close(); } catch (SQLException se2) { }// nothing we can
		 * do try { if (conn != null) conn.close(); } catch (SQLException se) {
		 * se.printStackTrace(); }// end finally try }// end try
		 */

		return null;
	}

	@Override
	public ResultSet getUser(Integer id) {

		try {
			conn = this.getConnection();
			stmt = (Statement) conn.createStatement();
			String sql = "select * from user where id=" + id;
			ResultSet rs = stmt.executeQuery(sql);
			return rs;
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} /*
		 * finally { // finally block used to close resources try { if (stmt !=
		 * null) stmt.close(); } catch (SQLException se2) { }// nothing we can
		 * do try { if (conn != null) conn.close(); } catch (SQLException se) {
		 * se.printStackTrace(); }// end finally try }// end try
		 */

		return null;

	}

	@Override
	public void release() {
		try {
			if (stmt != null)
				stmt.close();
		} catch (SQLException se2) {
		}// nothing we can do
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}//
	}

}
