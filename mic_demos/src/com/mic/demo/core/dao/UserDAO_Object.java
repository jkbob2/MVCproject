package com.mic.demo.core.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.mic.demo.core.model.User;

/**
 * 
 * 面向对象的用户数据访问接口
 * 
 * @author mulan
 * 
 */
@Component("userDAOMybatis")
public interface UserDAO_Object {
	/**
	 * 获取所有系统用户
	 * 
	 * 注：返回面向对象的结果，调用该接口的程序员仅需要了解对象User的属性即可
	 * 
	 * @return 系统用户列表
	 */
	public List<User> listUsers();

	public User getUser(@Param(value = "id") Integer id);
}
