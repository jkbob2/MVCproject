package com.mic.demo.core.dao;

import java.sql.ResultSet;

public interface UserDAO_ResultSet {
	
	
	/**
	 * 获取所有系统用户
	 * 
	 * 注：返回面向结果集的结果，调用该接口的程序员，需要理解该ResultSet的结构（列，和列的数据类型）
	 * 
	 * @return 系统用户列表
	 */
	public ResultSet listUsers();
	
	public ResultSet getUser(Integer id);
	
	/**
	 * 释放数据库链接
	 */
	public void release();

}
