package com.mic.demo.core.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class SpringJDBCSupport {

	private DriverManagerDataSource driverManagerDataSource;

	private DataSource dataSource;

	private JdbcTemplate jdbcTemplateObject;

	public SpringJDBCSupport() {

		String driverClassName = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://127.0.0.1:3306/mic_demos?useUnicode=true&characterEncoding=utf8";
		String username = "root";
		String password = "scut8711";

		driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName(driverClassName);
		driverManagerDataSource.setUrl(url);
		driverManagerDataSource.setUsername(username);
		driverManagerDataSource.setPassword(password);

		dataSource = driverManagerDataSource;
		jdbcTemplateObject = new JdbcTemplate(dataSource);

	}

	// getters and setters
	public DataSource getDataSource() {
		return dataSource;
	}

	public JdbcTemplate getJdbcTemplateObject() {
		return jdbcTemplateObject;
	}

}
