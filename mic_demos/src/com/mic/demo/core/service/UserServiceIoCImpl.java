package com.mic.demo.core.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mic.demo.core.dao.UserDAO_Object;
import com.mic.demo.core.model.User;
@Service("userService")
//Spring 注解，业务逻辑bean，运行时spring容器自动扫描该注解标签，将该类注册入spring bean容器
public class UserServiceIoCImpl implements UserService{

	// 程序员并不负责在编码阶段选定该接口的实现类
	// 即不需要在编码中使用new语句实例化一个具体的实现类
	// 在运行时，sprin bean容器将为该接口属性，根据配置文件要求动态赋值，即注入一个该接口的实现类例，
	// 如UserDAO_Object_Spring_JDBC_Impl
	// 上述行为即为控制反转和依赖注入 
	//（@Autowired标签，表示自动注入，@Qualifier表示需要注入的bean，或者使用@Resource(name="userDAOSpringJDBC")）
	//@Autowired
	//@Qualifier("userDAOJDBC")
	//@Qualifier("userDAOSpringJDBC")
	//@Qualifier("userDAOMybatis")
	@Resource(name="userDAOMybatis")
	private UserDAO_Object userDAO;

	@Override
	public List<User> listUsers() {
		// TODO Auto-generated method stub
		return userDAO.listUsers();
	}

	@Override
	public User getUser(Integer id) {
		// TODO Auto-generated method stub
		return userDAO.getUser(id);
	}
	
	// getters and setters
	
	public UserDAO_Object getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO_Object userDAO) {
		this.userDAO = userDAO;
	}


}
