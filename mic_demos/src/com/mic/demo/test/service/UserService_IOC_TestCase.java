package com.mic.demo.test.service;

import java.util.Iterator;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mic.demo.core.model.User;
import com.mic.demo.core.service.UserService;

import junit.framework.TestCase;

public class UserService_IOC_TestCase extends TestCase {

	private static ApplicationContext context;

	static {
		context = new ClassPathXmlApplicationContext(
				"classpath:configs/application-context.xml");
	}

	public void testUserServiceIoCImpl_listUsers() {

		UserService userService = (UserService) context.getBean("userService");

		List<User> users = userService.listUsers();
		Iterator iter = users.iterator();
		while (iter.hasNext()) {
			User user = (User) iter.next();
			System.out.println(user.toString());
		}
	}

	public void testUserServiceIoCImpl_getUser() {

		UserService userService = (UserService) context.getBean("userService");
		
		User user = userService.getUser(5038);
		System.out.println(user.toString());


	}

}
