package com.mic.demo.test.dao;

import java.util.Iterator;
import java.util.List;
import com.mic.demo.core.model.User;
import com.mic.demo.core.dao.UserDAO_Object;
import com.mic.demo.core.dao.UserDAO_Object_JDBC_Impl;
import junit.framework.TestCase;

public class UserDAO_Object_JDBC_Test extends TestCase {

	public void testUserDAO_Ojbect_JDBC_listUsers() {

		UserDAO_Object userDAO;
		try {
			userDAO = new UserDAO_Object_JDBC_Impl();
			List<User> users = userDAO.listUsers();
			Iterator<User> iter = users.iterator();
			while (iter.hasNext()) {
				User user = (User) iter.next();
				System.out.println(user.toString());
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}

	}

	public void testUserDAO_Ojbect_JDBC_getUser() {
		
		UserDAO_Object userDAO;
		try {
			userDAO = new UserDAO_Object_JDBC_Impl();
			User user = userDAO.getUser(5038);
			System.out.println(user.toString());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
