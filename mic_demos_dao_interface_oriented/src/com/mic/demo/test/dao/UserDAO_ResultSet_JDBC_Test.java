package com.mic.demo.test.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.mic.demo.core.dao.UserDAO_ResultSet;
import com.mic.demo.core.dao.UserDAO_ResultSet_JDBC_Impl;
import com.mic.demo.core.model.User;

import junit.framework.TestCase;

public class UserDAO_ResultSet_JDBC_Test extends TestCase {

	public void testUserDAO_ResultSet_JDBC_listUsers() {


		UserDAO_ResultSet userDAO;
		try {
			userDAO = new UserDAO_ResultSet_JDBC_Impl();
			ResultSet rs = userDAO.listUsers();
			while (rs.next()) {
				// 创建User实例，并从结果集中获取数据，设置它的属性
				User user = new User();
				Integer id = rs.getInt("id");
				user.setId(id);
				String username = rs.getString("username");
				user.setUsername(username);
				String showname = rs.getString("showname");
				user.setShowname(showname);
				String password = rs.getString("password");
				user.setPassword(password);
				String question = rs.getString("question");
				user.setQuestion(question);
				String answer = rs.getString("answer");
				user.setAnswer(answer);
				String rolecode = rs.getString("rolecode");
				user.setRolecode(rolecode);
				Date lastlogintime = rs.getDate("lastlogintime");
				user.setLastlogintime(lastlogintime);
				boolean enabled = rs.getBoolean("enabled");
				user.setEnabled(enabled);
				boolean accountNonExpired = rs.getBoolean("accountNonExpired");
				user.setAccountNonExpired(accountNonExpired);
				boolean credentialsNonExpired = rs
						.getBoolean("credentialsNonExpired");
				user.setCredentialsNonExpired(credentialsNonExpired);
				boolean accountNonLocked = rs.getBoolean("accountNonLocked");
				user.setAccountNonLocked(accountNonLocked);

				System.out.println(user.toString());
			}
			// 程序员必须主动释放数据库链接
			userDAO.release();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void testUserDAO_ResultSet_JDBC_getUser() {

		UserDAO_ResultSet userDAO;
		try {
			userDAO = new UserDAO_ResultSet_JDBC_Impl();
			ResultSet rs = userDAO.getUser(5038);
			if (rs.next()) {
				// 创建User实例，并从结果集中获取数据，设置它的属性
				User user = new User();
				Integer id = rs.getInt("id");
				user.setId(id);
				String username = rs.getString("username");
				user.setUsername(username);
				String showname = rs.getString("showname");
				user.setShowname(showname);
				String password = rs.getString("password");
				user.setPassword(password);
				String question = rs.getString("question");
				user.setQuestion(question);
				String answer = rs.getString("answer");
				user.setAnswer(answer);
				String rolecode = rs.getString("rolecode");
				user.setRolecode(rolecode);
				Date lastlogintime = rs.getDate("lastlogintime");
				user.setLastlogintime(lastlogintime);
				boolean enabled = rs.getBoolean("enabled");
				user.setEnabled(enabled);
				boolean accountNonExpired = rs.getBoolean("accountNonExpired");
				user.setAccountNonExpired(accountNonExpired);
				boolean credentialsNonExpired = rs
						.getBoolean("credentialsNonExpired");
				user.setCredentialsNonExpired(credentialsNonExpired);
				boolean accountNonLocked = rs.getBoolean("accountNonLocked");
				user.setAccountNonLocked(accountNonLocked);

				System.out.println(user.toString());
			}
			// 程序员必须主动释放数据库链接
			userDAO.release();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
