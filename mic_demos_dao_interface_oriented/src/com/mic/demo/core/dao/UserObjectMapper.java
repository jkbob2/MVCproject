package com.mic.demo.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import org.springframework.jdbc.core.RowMapper;
import com.mic.demo.core.model.User;

/**
 * 提供从结果集到对象实例的转化（ResultSet-Object Mapper)
 * 
 * 从代码可维护性角度，比JDBC技术提高一些～～
 * 
 * 如果user增加性别属性呢？ 需要修改那些地方？
 * 
 * @author mulan
 *
 */
public class UserObjectMapper implements RowMapper<User> {
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		User user = new User();
		Integer id = rs.getInt("id");
		user.setId(id);
		String username = rs.getString("username");
		user.setUsername(username);
		String showname = rs.getString("showname");
		user.setShowname(showname);
		String password = rs.getString("password");
		user.setPassword(password);
		String question = rs.getString("question");
		user.setQuestion(question);
		String answer = rs.getString("answer");
		user.setAnswer(answer);
		String rolecode = rs.getString("rolecode");
		user.setRolecode(rolecode);
		Date lastlogintime = rs.getDate("lastlogintime");
		user.setLastlogintime(lastlogintime);
		boolean enabled = rs.getBoolean("enabled");
		user.setEnabled(enabled);
		boolean accountNonExpired = rs.getBoolean("accountNonExpired");
		user.setAccountNonExpired(accountNonExpired);
		boolean credentialsNonExpired = rs
				.getBoolean("credentialsNonExpired");
		user.setCredentialsNonExpired(credentialsNonExpired);
		boolean accountNonLocked = rs.getBoolean("accountNonLocked");
		user.setAccountNonLocked(accountNonLocked);

		return user;
	}
}
