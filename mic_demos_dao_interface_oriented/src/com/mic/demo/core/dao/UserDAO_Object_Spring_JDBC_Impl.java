package com.mic.demo.core.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mic.demo.core.model.User;

/**
 * 面向对象的用户数据访问接口 使用Spring JDBC技术实现
 * 
 * @author mulan
 * 
 */
public class UserDAO_Object_Spring_JDBC_Impl extends SpringJDBCSupport implements
		UserDAO_Object {

	
	public UserDAO_Object_Spring_JDBC_Impl(){
		super();
	}
	
	@Override
	public List<User> listUsers() {
		String SQL = "select * from user";
		List<User> users = this.getJdbcTemplateObject().query(SQL,
				new UserObjectMapper());
		return users;
	}

	@Override
	public User getUser(Integer id) {
		String SQL = "select * from user where id = ?";
		User user = this.getJdbcTemplateObject().queryForObject(SQL,
				new Object[] { id }, new UserObjectMapper());
		return user;
	}

}
